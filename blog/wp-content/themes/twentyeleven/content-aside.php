<div id="post-<?php the_ID(); ?>" class="post grid_12 alpha omega">
    <div class="image grid_3 alpha">
        <img src="images/image.jpg" alt="" />
    </div>
    <div class="grid_9 omega">
        <h2 class="pink font"><?php
		$categories = get_the_category();
		$seperator = ' - ';
		$output = '';
		if($categories){
			foreach($categories as $category) {
				$output .= '<a href="'.get_category_link($category->term_id ).'" title="' . esc_attr( sprintf( __( "View all posts in %s" ), $category->name ) ) . '">'.$category->cat_name.'</a>'.$seperator;
			}
		echo trim($output, $seperator);
		}
		?>
		</h2>
        <h3 class="font"><?php the_title(); ?></h3>
        <ul class="post_info">
            <li class="author font"><?php the_author(); ?> 
			<?php if ( get_the_author_meta('description') ){
				echo '('.get_the_author_meta('description').')';
			} ?></li>
            <li class="date font"><?php the_date(); ?></li>
            <li class="comments font"><a class="dark" href="#"><?php
			global $wpdb;
			$postID = get_the_ID();
			$comments = $wpdb->get_row("SELECT comment_count as count FROM wp_posts WHERE ID = '$postID'");
			$commentcount = $comments->count;
			if($commentcount == 1): $commenttext = 'comment'; endif;
			if($commentcount > 1 || $commentcount == 0): $commenttext = 'comments'; endif;
			$fulltitle = $commentcount.' '.$commenttext;
			echo $fulltitle;
			?></a></li>
        </ul>
        <div class="clear"></div>
        <div class="separator half">&nbsp;</div>
        <p class="large font"><?php the_content(); ?>
            <a class="readmore medium" href="#">Read more</a>
        </p>
    </div>
</div>
<div class="clear"></div>
<div class="separator">&nbsp;</div>