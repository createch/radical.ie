<?php
/**
 * The default template for displaying content
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */
?>
<div id="post-<?php the_ID(); ?>" class="post grid_12 alpha omega">
	<?php if ( has_post_thumbnail() ) { ?>
    <div class="image grid_3 alpha">
        <?php the_post_thumbnail(); ?>
    </div>
    <div class="grid_9 omega">
    <?php } else { ?>
		<div class="grid_12 alpha omega">
	<?php } ?>
    	<h2 class="pink font"><?php
		$categories = get_the_category();
		$seperator = ', ';
		$output = '';
		if($categories){
			foreach($categories as $category) {
				$output .= '<a class="pink font" href="'.get_category_link($category->term_id ).'" title="' . esc_attr( sprintf( __( "View all posts in %s" ), $category->name ) ) . '">'.$category->cat_name.'</a>'.$seperator;
			}
		echo trim($output, $seperator);
		}
		?>
		</h2>
        <h3 class="font"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
        <ul class="post_info">
            <li class="author font"><?php the_author(); ?> 
			<?php if ( get_the_author_meta('description') ){
				echo '('.get_the_author_meta('description').')';
			} ?></li>
            <li class="date font"><?php the_date(); ?></li>
            <li class="comments font"><a class="dark" href="<?php the_permalink(); ?>#comments"><?php
			global $wpdb;
			$postID = get_the_ID();
			$comments = $wpdb->get_row("SELECT comment_count as count FROM wp_posts WHERE ID = '$postID'");
			$commentcount = $comments->count;
			if($commentcount == 1): $commenttext = 'comment'; endif;
			if($commentcount > 1 || $commentcount == 0): $commenttext = 'comments'; endif;
			$fulltitle = $commentcount.' '.$commenttext;
			echo $fulltitle;
			?></a></li>
        </ul>
        <div class="clear"></div>
        <div class="separator half dashed">&nbsp;</div>
        <p class="large font"><?php $content = strip_tags(do_shortcode( get_the_content() )); echo substr($content, 0, 300).'...'; ?>
			<a class="readmore medium" href="<?php the_permalink(); ?>">Read more</a>
        </p>
    </div>
</div>
<div class="clear"></div>
<div class="separator">&nbsp;</div>