<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */
?>
	<div class="clear"></div>
	</div><!-- #main -->
    <div class="clear"></div>
</div><!-- #page -->
<div id="footer">
	<div class="separator">&nbsp;</div>
    <div class="container_16">
        <div class="grid_16 alpha omega">
            <h5>&nbsp;</h5><h5>&nbsp;</h5>
        </div>
        <div class="clear"></div>
    </div>
</div>

<?php wp_footer(); ?>

</body>
</html>