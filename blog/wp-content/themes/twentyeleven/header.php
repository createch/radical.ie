<!DOCTYPE HTML>
<html>
<head>
<meta charset="UTF-8">
<title><?php
	/*
	 * Print the <title> tag based on what is being viewed.
	 */
	global $page, $paged;

	wp_title( '|', true, 'right' );

	// Add the blog name.
	bloginfo( 'name' );

	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";

	// Add a page number if necessary:
	if ( $paged >= 2 || $page >= 2 )
		echo ' | ' . sprintf( __( 'Page %s', 'twentyeleven' ), max( $paged, $page ) );

	?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<link href="<?php bloginfo('template_url'); ?>/css/reset.css" rel="stylesheet" type="text/css" />
<link href="<?php bloginfo('template_url'); ?>/css/grid.css" rel="stylesheet" type="text/css" />
<link href="<?php bloginfo('template_url'); ?>/css/style.css" rel="stylesheet" type="text/css" />
<script src="<?php bloginfo('template_url'); ?>/js/jquery.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/font.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/main.js"></script>
<script type="text/javascript">Cufon.replace('.font', { hover: true, hoverables: { li: true, ul:true, a: true, p: true } });</script>
<?php
	/* We add some JavaScript to pages with the comment form
	 * to support sites with threaded comments (when in use).
	 */
	if ( is_singular() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );

	/* Always have wp_head() just before the closing </head>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to add elements to <head> such
	 * as styles, scripts, and meta tags.
	 */
	wp_head();
?>
</head>
<script type="text/javascript">
 
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-37421292-1']);
  _gaq.push(['_trackPageview']);
 
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
 
</script>
<body <?php body_class(); ?>>
    <div id="header_wrapper">
        <div id="header" class="container_16">
            <div id="logo" class="grid_16">
                <div class="grid_4 alpha">
                    <h1>Radical</h1>&nbsp;
                </div>
                <div class="grid_12 omega nav_height">
                    <ul id="navigation" class="font">
                        <?php wp_nav_menu( array( 'theme_location' => 'primary','menu'=>'category-menu' ) ); ?>
                    </ul>
                </div>
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <div id="main" class="container_16">
        <div class="grid_16">