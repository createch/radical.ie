<?php
/**
 * The template for displaying Search Results pages.
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */


get_header(); ?>
		<?php get_sidebar(); ?>
		<div id="post_list" class="grid_12 omega">
			<header class="page-header">
					<h2 class="dark"><?php printf( __( 'Search results for: %s', 'twentyeleven' ), '<span>' . get_search_query() . '</span>' ); ?></h2>
					<div class="separator dashed">&nbsp;</div>
				</header>
			<?php if ( have_posts() ) : ?>
				<?php /* Start the Loop */ ?>
				<?php while ( have_posts() ) : the_post(); ?>
					<?=get_post_format();?>
					<?php get_template_part( 'content', get_post_format() ); ?>

				<?php endwhile; ?>

				<?php twentyeleven_content_nav( 'nav-below' ); ?>
			<?php else : ?>
            	<h2 class="dark"><?php _e( 'Search Results', 'twentyeleven' ); ?></h2>
            	<p><?php _e( 'Sorry, but nothing matched your search criteria. Please try again with some different keywords.', 'twentyeleven' ); ?></p>
			<?php endif; ?>
			</div><!-- #content -->
<?php get_footer(); ?>