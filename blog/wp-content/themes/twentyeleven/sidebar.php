<?php
/**
 * The Sidebar containing the main widget area.
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 *

$options = twentyeleven_get_theme_options();
$current_layout = $options['theme_layout'];

if ( 'content' != $current_layout ) :
?>
		<div id="secondary" class="widget-area" role="complementary">
			<?php if ( ! dynamic_sidebar( 'sidebar-1' ) ) : ?>

				<aside id="archives" class="widget">
					<h3 class="widget-title"><?php _e( 'Archives', 'twentyeleven' ); ?></h3>
					<ul>
						<?php wp_get_archives( array( 'type' => 'monthly' ) ); ?>
					</ul>
				</aside>

				<aside id="meta" class="widget">
					<h3 class="widget-title"><?php _e( 'Meta', 'twentyeleven' ); ?></h3>
					<ul>
						<?php wp_register(); ?>
						<li><?php wp_loginout(); ?></li>
						<?php wp_meta(); ?>
					</ul>
				</aside>

			<?php endif; // end sidebar widget area ?>
		</div><!-- #secondary .widget-area -->
<?php endif; ?>
*/ ?>
<div id="sidebar" class="grid_4 alpha">
            	<div id="search" class="section">
                    <div class="content">
                    	<input type="text" name="search" value="" class="input" />
                        <a href="Javascript: void(0);" id="search_button" class="submit"></a>
                    </div>
                    <div class="separator half">&nbsp;</div>
                </div>
                <div class="section">
                	<h2 class="font">Other links</h2>
                    <div class="content">
                    	<ul class="pink disc">
                        	<li><span class="dark font"><a class="dark" target="_blank" href="http://www.radical.ie">Radical homepage</a></span></li>
                            <li><span class="dark font"><a class="dark" target="_blank" href="http://www.facebook.com/RadicalDublin">Facebook</a></span></li>
                            <li><span class="dark font"><a class="dark" target="_blank" href="http://twitter.com/RadicalDublin">Twitter</a></span></li>
                        </ul>
                    </div>
                    <div class="separator half">&nbsp;</div>
                </div>
                <div class="section">
                	<h2 class="font">Tags</h2>
                    <div class="content">
                    	<ul id="tags">
                        <?php $args = array('unit'=>'px','smallest'=>14,'largest'=> 14,'format'=> 'array','number'=>20,'order'=>'RAND','separator'=>'::'); $tags=wp_tag_cloud( $args ); 
							foreach($tags as $tag){
						?> 
                        	<li class="font"><?=$tag?></li>
                    	<?php } ?> 
                        <div class="clear"></div>
                    </div>
                    <div class="separator half">&nbsp;</div>
                </div>
                <div id="archive" class="section">
                	<h2>Archive</h2>
                    <div class="content">
                    	<ul id="months" class="pink disc">
                        	<?php wp_get_archives( array( 'type' => 'monthly','limit'=>10 ) ); ?>
                        </ul>
                    </div>
                </div>
            </div>