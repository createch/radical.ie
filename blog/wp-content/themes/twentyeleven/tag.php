<?php
/**
 * The template used to display Tag Archive pages
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */

get_header(); ?>
		<?php get_sidebar(); ?>
		<div id="post_list" class="grid_12 omega">
			<header class="page-header">
					<h2 class="dark"><?php printf( __( 'Tag: %s', 'twentyeleven' ), '<span>' . single_tag_title( '', false ) . '</span>' ); ?></h2>
					<div class="separator dashed">&nbsp;</div>
					<?php
						$tag_description = tag_description();
						if ( ! empty( $tag_description ) )
							echo apply_filters( 'tag_archive_meta', '<div class="tag-archive-meta">' . $tag_description . '</div>' );
					?>
				</header>
			<?php if ( have_posts() ) : ?>
				<?php /* Start the Loop */ ?>
				<?php while ( have_posts() ) : the_post(); ?>
					<?=get_post_format();?>
					<?php get_template_part( 'content', get_post_format() ); ?>

				<?php endwhile; ?>

				<?php twentyeleven_content_nav( 'nav-below' ); ?>
			<?php else : ?>
            	<h2 class="dark"><?php _e( 'Nothing Found', 'twentyeleven' ); ?></h2>
            	<p><?php _e( 'Apologies, but no results were found for the requested archive. Perhaps searching will help find a related post.', 'twentyeleven' ); ?></p>
			<?php endif; ?>
			</div><!-- #content -->
<?php get_footer(); ?>
