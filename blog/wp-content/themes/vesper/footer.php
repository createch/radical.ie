
		</div>
	<div id="footer_wrap">
		<div id="footer">
			<?php bloginfo('name'); ?> is proudly powered by <a href="http://wordpress.org/" target="_blank">WordPress</a> | Designed by: <a href="http://www.valendesigns.com/" target="_blank">Valen Designs LLC</a>
            <div id="rss"> 
				<a href="<?php bloginfo('rss2_url'); ?>" id="rss-entries" title="rss entries">rss entries</a>
				<a href="<?php bloginfo('comments_rss2_url'); ?>" id="rss-comments" title="rss comments">rss comments</a> 
			</div>
		</div>
    </div>

		<?php wp_footer(); ?>
	</body>
<script type="text/javascript">
	var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
	document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
	try {
		var pageTracker = _gat._getTracker("UA-244814-11");
		pageTracker._trackPageview();
	} catch(err) {}</script>

</html>
