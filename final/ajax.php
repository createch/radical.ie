<?php

include('php/Entry.php');
include('php/Twitter.php');
// This variable always outputting at the end of the doc
$output='No action called!';

if($_POST['action']){
	$action=$_POST['action'];
	/*
	* Loads content from a file (see getPage() in main.js)
	*/
	if($action=='get_page'){
		if($_POST['page']){
			$output=file_get_contents('html/'.$_POST['page'].'.html');
		} else {
			$output='The requested content does not exist!';
		}
		
	} else if($action=='getTwitterStats'){
		$tw=new Twitter(array(array('feed'=>'radicaldublin')));
		$tweets=$tw->get('tweets');
		$output=$tweets[0]['text'].'::'.$tw->get('statuses_count').'::'.$tw->get('followers_count').'::'.$tw->get('friends_count');
	}
}
echo $output;