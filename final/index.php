<?php
// ** MySQL settings ** //
define('DB_NAME', 'radical_wordpress');    // The name of the database
define('DB_USER', 'radical_wusr');     // Your MySQL username
define('DB_PASSWORD', '2P4HU8xq'); // ...and password
define('DB_HOST', 'localhost');    // 99% chance you won't need to change this value

$con = mysql_connect(DB_HOST,DB_USER,DB_PASSWORD);
mysql_select_db(DB_NAME, $con);

$posts=array();
$result = mysql_query("SELECT ID,post_title,guid FROM wp_posts WHERE post_type='post' AND post_status='publish' ORDER BY ID DESC LIMIT 3");
while($row = mysql_fetch_array($result)){
	$result2 = mysql_query("SELECT * FROM wp_posts WHERE post_id='".$row['ID']."' AND meta_key = '_wp_attached_file'");
	$posts[]=$row;
}


?>
<!DOCTYPE HTML>
<html class="no-js">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;"/>
<title>Radical</title>
<link href="css/reset.css" rel="stylesheet" type="text/css" />
<link href="css/grid.css" rel="stylesheet" type="text/css" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script src="js/jquery.js"></script>
<script src="js/main.js"></script>
</head>
<body>
	<div id="header">
        <div id="nav_container" class="container_12">
            <header>
                <div id="logo">
                    <a rel="page-1" href="Javascript: void(0);"><img src="images/logo.png" alt="radical" /></a>
                </div>
                <div id="navigation">
                	<nav>
                        <ul>
                            <li><a rel="page-3" class="green" href="Javascript: void(0);">Search &amp; Analytics</a></li>
                            <li><a rel="page-4" class="green pink-bullet" href="Javascript: void(0);">Digital Media</a></li>
                            <li><a rel="page-5" class="green pink-bullet" href="Javascript: void(0);">Social</a></li>
                            <li><a rel="page-6" class="green pink-bullet" href="Javascript: void(0);">Creative Technology</a></li>
                            <li><a href="http://www.radical.ie/blog">Blog</a></li>
                            <li><a rel="page-7" href="Javascript: void(0);">About us</a></li>
                            <li><a rel="page-8" href="Javascript: void(0);">Get in touch</a></li>
                        </ul>
                        <select>
                        	<option value="">Go &gt;&gt;</option>
                        	<option value="page-1">Home</option>
                            <option value="page-2">What we do</option>
                            <option value="page-3">- Search &amp; Analytics</option>
                            <option value="page-4">- Digital Media</option>
                            <option value="page-5">- Social</option>
                            <option value="page-6">- Creative Technology</option>
                            <option value="blog">Blog</option>
                            <option value="page-7">About us</option>
                            <option value="page-8">Get in touch</option>
                        </select>
                	</nav>
                </div>
            </header>
        </div>
    </div>
    <div id="page-1">
    	<div class="container_12">
            <div class="middle">
                <img id="big_r" src="images/big_r.png" alt="Image of the big R for Radical" />
                <h2>We try to make it look easy</h2>
                <p>The new digital universe can be a complicated jungle for brands. The rate of technological change combined with a shifting consumer mindset can be bewildering for anyone. At Radical, we put a lot of hard work into producing data-led consumer insights. We distill the most up-to-the-minute information about your consumers into a simple user-friendly form. And we do it calmly and clearly.</p>
                <h3 class="big_top_margin">Online Media sector to grow by over 12% in 2012</h3>
                <p>"By the end of 2013 there will be as many tablets in the Irish market as the combined readership of the Irish Independent and Irish Times"</p>
                <h4><a class="arrow" target="_blank" href="http://www.radical.ie/pdf/Outlook2012_radical.pdf">Read Radical's media outlook for 2012</a></h4>
            </div>
            <div class="divider"></div>
            <div class="boxes">
            	<div id="twitter" class="box first">
                	<h3>@radicaldublin</h3>
                	<div class="content">
                    	<div class="margin">
                        	<div id="tweet"><div class="loading"><img src="images/loader.gif" alt="Loading..." /></div></div>
                            <div id="twitter_stats">
                                <ul>
                                    <li class="first"><h5 class="prox">Tweets</h5><div id="stat1" class="chunk">0</div></li>
                                    <li><h5 class="prox">Followers</h5><div id="stat2" class="chunk">0</div></li>
                                    <li class="last"><h5 class="prox">Following</h5><div id="stat3" class="chunk">0</div></li>
                                </ul>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="blog" class="box middle">
                	<h3>Latest from our blog</h3>
                	<div class="content">
                    	<div class="margin">
                        <?php for($q=0;$q<sizeof($posts);$q++){ ?>
                        	<div class="blog_entry">
                            	<?php //<div class="image"></div> ?>
                            	<a target="_blank" href="<?=$posts[$q]['guid'];?>"><h4 class="prox"><?=$posts[$q]['post_title'];?></h4></a>
                                <div class="clear"></div>
                            </div>
                            <?php if($q<(sizeof($posts)-1)){ ?><div class="divider"></div><?php } ?>
						<?php } ?>
                        </div>
                    </div>
                </div>
                <div id="stats" class="box last">
                	<h3>Every day we...</h3>
                	<div class="content">
                    	<div class="margin">
                        	<div class="sidebar"></div>
                        	<div class="stat" style="width:95%;"><p><span class="green">Buy</span> 4,657,534 Page Impressions</p></div>
                            <div class="stat" style="width:90%;"><p><span class="green">Manage</span> 57 Social Media sites managed</p></div>
                            <div class="stat" style="width:95%;"><p><span class="green">Reach</span> 48% of Irish Facebook Audience</p></div>
                            <div class="stat" style="width:90%;"><p><span class="green">In</span> 25 Countries</p></div>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </div>
    <div id="page-2">
        <div class="container_12">
            <div class="title"><img src="images/titles/what.png"></div>
            <p>Digital can seem like an intimidating and unknowable beast. Yet it is still all about communication.</p>
            <p>It would, however, be wrong to categorise digital as simply another marketing channel. It is markedly different, and success in digital is based on embracing that difference. Radical see digital as a blend of many skills and disciplines (seven, to be precise) and achieving balance within these disciplines drives success.</p>
            <div class="bottom">
                <img class="image" src="images/titles/bottom.png">
                <img src="images/backgrounds/whatwedo-content.png">	
            </div>
        </div><!--.story-->
    </div> <!--#second-->
    <div id="page-3">
        <div class="container_12">
            <div class="title"><img src="images/titles/search.png"></div>
            <p>Our Search Team aim high; exceeding expectations on a regular basis. We do that by restlessly refining our implementation methodology across the core areas of PPC, SEO and Analytics. </p>
            <p>As well as being AdWords Qualified, we’re one of only two Certified Partners for Google Analytics in Ireland. That means we not only drive traffic but analyse it too- allowing us make informed decisions about how customers navigate and use your site.</p>
            <p>Our approach is 100% measurable and recorded in reports on an ongoing basis.  That’s not as much of a chore as it might sound; few things give us more pleasure than seeing improvements to our clients’ sites- and to watch their business grow as a result.</p>
        	<div class="bottom">
                <img class="image" src="images/titles/bottom.png">
                <img src="images/backgrounds/search-content.png">	
            </div>
        </div> <!--.story-->
    </div> <!--#third-->   
    <div id="page-4">
        <div class="container_12">
            <div class="title"><img src="images/titles/digital.png"></div>
            <p>We understand online advertising. More than that, we drive it. Our specialist digital media team carries out every facet of a campaign including planning, buying and optimization. That gives us ultimate control and flexibility in ensuring we meet all our clients’ objectives.</p>
            <p>We thrive on the insights that this data can yield and use them to make our campaigns more successful. Innovation is in our DNA; we always strive to make an Irish media ‘first’ and to bring innovation to our clients at every turn.</p>
            <p>Integration is such a key word here that we’ve integrated it. </p>
            <p>From planning and buying to negotiation and sponsorship management, Digital media is painstakingly aligned with all other brand activity, both online and offline.</p>
            <div class="bottom">
                <img class="image" src="images/titles/bottom.png">
                <img src="images/backgrounds/digital-content.png">	
            </div>
        </div> <!--.story-->
    </div> <!--#third-->
	<div id="page-5">
        <div class="container_12">
            <div class="title"><img src="images/titles/social.png"></div>
            <p>Social Media has woven its way into the life of the consumer, much welcomed and much used. At Radical we have a team of four social media advocates with diverse experience and specialities.  From social listening research to training and community management, there’s little we don’t offer. Our understanding of this dynamic environment means we’re well placed to create and manage the conversation between your brand and your customers.</p>
            <div class="bottom">
                <img class="image" src="images/titles/bottom.png">
                <img src="images/backgrounds/social-content.png">	
            </div>
        </div> <!--.story-->
    </div> <!--#fifth-->
    <div id="page-6">
        <div class="container_12">
            <div class="title"><img src="images/titles/web.png"></div>
            <p>Throw a stick in Radical and you’ll hit project managers, content strategists, copywriters, usability specialists, UX designers, technology experts or developers. That’s because we’re one of the few Digital Media agencies in Ireland to offer a full-service creative and development team in-house. </p>
        	<p>We design and develop websites on a range of CMS platforms, mobile apps, Facebook and social apps. Working alongside the other teams at Radical means our creative work not only looks good and stands out, but is informed by the very latest and best practice technology and trends in digital media, social media and search &amp; analytics. </p>
        	<div class="bottom">
                <img class="image" src="images/titles/bottom.png">
            	<img src="images/backgrounds/web-content.png">
            </div>
        </div> <!--.story-->
    </div> <!--#sixth-->
    <div id="page-7">
        <div class="container_12">
            <div class="title"><img src="images/titles/about.png"></div>
            <p>Radical is a dynamic business at a dynamic time; we’ve made great strides over the last few years and have plenty more to do to achieve our vision. That vision? To be the agency that clients entrust with all their digital strategies, an agency that defines and refines those strategies through insight, creativity and innovation. Digital is in a constant state of evolution. Its influence is set to grow as it becomes more and more woven into the everyday life of the consumer. It is the 'king maker' of marketing communications and, over the coming decade, will be the single most important element of the advertising and media industry. That represents an opportunity for us all.</p>
            <p>The engine room of Radical is our team: nearly 30 digital passionistas striving to achieve the highest standards of digital marketing for our clients. A team with a wide variety of skills and experience to bring to the table. A team that delivers time after time.</p>
        	<p>Radical is part of the Core Media Group and this creates access to some of the best minds in the media business- bringing insight and media buying power beyond the digital world.</p>
        	<div class="bottom">
                <img class="image" src="images/titles/bottom.png">
                <img src="images/backgrounds/core-logo.png">
            </div>
        </div> <!--.story-->
    </div> <!--#seventh-->
    <div id="page-8">
        <div class="container_12">
        	<div class="title"><img src="images/titles/contact.png"></div>
            <a href="http://maps.google.ie/maps?q=16+Sir+John+Rogerson%27s+Quay,+Dublin&hl=en&ll=53.346401,-6.244097&spn=0.009979,0.023818&sll=53.346421,-6.244171&sspn=0.001254,0.002977&hnear=16+Sir+John+Rogerson%27s+Quay,+Dublin+2,+County+Dublin&t=m&z=16" target="_blank"><img src="images/backgrounds/map.png"></a>
            <br>
            <p><strong>Address:</strong> <br>
            16 Sir John Rogersons Quay,<br>Dublin 2<br><br>
            <strong>Phone:</strong> +353 (0) 1 649 6301<br>
            <strong>Email:</strong> <a href="mailto:info@radical.ie">info@radical.ie</a><br>
            <strong>Twitter:</strong> <a href="http://twitter.com/#!/radicaldublin">@radicaldublin</a></p>
            <br>
            <br>
            <p>To find out how we can help you, contact Justin Cullen, Managing Director</p><br>
            <p><strong>Phone:</strong> +353 (0) 1 649 6397<br>
            <strong>Email:</strong> <a href="mailto:Justin.cullen@radical.ie">Justin.cullen@radical.ie</a></strong></p>
        </div> <!--.story-->
    </div><!--#tenth-->
</body>
</html>
