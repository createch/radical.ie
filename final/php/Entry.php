<?php
	class Entry {
		
		private $table,$id,$timestamp;
		private $vars=array();
		private $unique=array();
		private $errors=array();
		
		public function __construct($setVars=NULL){
			$this->table=get_class($this);
			if($setVars){ $this->set($setVars); }
		}
		
		public function get($v){
			if(strtoupper($v)=='ID'){
				return $this->id;
			} else if(strtoupper($v)=='TIMESTAMP'){
				return $this->timestamp;
			} else {  
				if($this->vars[$v]){
					return $this->vars[$v];
				} else {
					return NULL;
				}
			}
		}
		
		public function set($atts){
			$keys=array_keys($atts);
			for($q=0;$q<sizeof($atts);$q++){
				if(strtoupper($keys[$q])=='ID'){
					$this->id=$atts[$keys[$q]];
				} else if(strtoupper($keys[$q])=='TIMESTAMP'){
					$this->timestamp=$atts[$keys[$q]];
				} else {
					if(strtoupper($keys[$q])=='PASS'||strtoupper($keys[$q])=='PASSWORD'){
						$this->vars[$keys[$q]]=''.md5($atts[$keys[$q]]).'';
					} else {
						$this->vars[$keys[$q]]=''.$atts[$keys[$q]].'';
					}
				}
			}
		}
		
		public function delete(){
			$sql="DELETE FROM ".$this->table." WHERE id=".mysql_real_escape_string($this->get('id'));
			if(mysql_query($sql)){
				return true;
			} else {
				return false;
			}
		}
		
		public function save($vars=array()){	
			if(sizeof($vars)>0){
				$this->set($vars);	
			}
			$this->createTable();
			$this->checkColumns();
			
			if(!$this->checkUniques()){
			
				if(!$this->id){
					$sql="INSERT INTO ".$this->table." (";
					$keys=array_keys($this->vars);
					for($q=0;$q<sizeof($keys);$q++){
							if($q!=0){ $sql.=",";}
							$sql.=mysql_real_escape_string($keys[$q]);
					}
					$sql.=") VALUES (";
					for($w=0;$w<sizeof($this->vars);$w++){
							if($w!=0){ $sql.=",";}
							$sql.="'".mysql_real_escape_string($this->vars[$keys[$w]])."'";
					}
					$sql.=")";
					if(mysql_query($sql)){
						return true;
					} else {
						return false;
					}
				} else {
					$sql="UPDATE ".$this->table;
					$keys=array_keys($this->vars);
					for($q=0;$q<sizeof($keys);$q++){
						if($q==0){ $sql.=" SET "; } 
						else {$sql.=",";}
						$sql.=mysql_real_escape_string($keys[$q])."='".mysql_real_escape_string($this->vars[$keys[$q]])."'";
					}
					$sql.=" WHERE id=".$this->id;
					if(mysql_query($sql)){
						return true;
					} else {
						return false;
					}
				}
			} else {
				return false;	
			}
		}
		
		private function createTable(){
			$sql="CREATE TABLE IF NOT EXISTS ".$this->table." (
				`id` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
				`timestamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
				) ENGINE = MYISAM ;";
			mysql_query($sql);	
		}
		
		private function checkColumns(){
			$keys=array_keys($this->vars);
			foreach($keys as $key){
				if(!$this->colExists($key)){
					$this->createCol($key);
				}
			}
		}
		
		private function colExists($col){
			$sql="SHOW COLUMNS FROM ".$this->table." LIKE  '".$col."'";
			if(mysql_num_rows(mysql_query($sql))==0){
				return false;
			} else {
				return true;	
			}
		}
		
		private function createCol($col,$datatype=NULL){
			if($datatype){
				$sql="ALTER TABLE ".$this->table." ADD ".$col." ".$datatype." NULL";
			} else {
				$sql="ALTER TABLE ".$this->table." ADD ".$col." varchar(255) NULL";
			}
			if(mysql_query($sql)){
				return true;
			} else{
				return false;	
			}
		}
		
		private function checkUniques(){
			$exists=false;
			foreach($this->unique as $u){
				if($this->vars[$u]){
					$sql="SELECT * FROM ".$this->table." WHERE ".$u."='".$this->vars[$u]."'";
					if(mysql_num_rows(mysql_query($sql))>0){
						$this->errors[]=$u;
						$exists=true;
					}
				}
			}
			return $exists;
		}
		
		public function getLastError(){
			if(sizeof($this->errors)!=0){
				return $this->errors[sizeof($this->errors)-1];
			} else {
				return 'No errors exist!';	
			}
		}
		
		public function setUnique($u){
			$this->unique[]=$u;
		}
		
	}
?>