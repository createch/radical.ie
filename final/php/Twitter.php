<?php class Twitter extends Entry {
	
	private $tweets=array();
	
	//unset functions
	public function save($vars=NULL){
		echo 'save function is disabled for this class!';
	}
	//
	
	public function __construct($vars=NULL){
		$this->set(array(
			'api_user_url'=>'http://api.twitter.com/1/users/show.json?screen_name=',
			'api_tweets_url'=>'http://api.twitter.com/1/statuses/user_timeline/',
			'feed'=>'RadicalDublin',
			'count'=>10
		));	
		$this->table=get_class($this);
		if($vars){ $this->set($vars); }
		$this->load();
	}
	
	public function get($v){
		if($v=='tweets'){
			return $this->tweets;	
		} else {
			return parent::get($v);
		}
	}
	
	private function load(){	
		if($this->initUser(file_get_contents($this->get('api_user_url').$this->get('feed')))&&$this->initTweets(file_get_contents($this->get('api_tweets_url').$this->get('feed').'.json?include_rts=true&count='.$this->get('count')))){
			return true;
		} else {
			echo 'An error has occurred';
			return false;
		}
	}
	
	private function initUser($data){
		if($data=json_decode($this->prepareJSON($data),true)){
			$this->set(array(
				'name'=>$data['name'],
				'location'=>$data['location'],
				'url'=>$data['url'],
				'description'=>$data['description'],
				'followers_count'=>$data['followers_count'],
				'friends_count'=>$data['friends_count'],
				'statuses_count'=>$data['statuses_count'],
				'images'=>$data['profile_image_url']
			));
			return true;
		} else {
			return false;	
		}
	}
	
	private function initTweets($data){
		if($data=json_decode($this->prepareJSON($data),true)){
			for($q=0;$q<sizeof($data);$q++){
				$date=explode(' ',$data[$q]['created_at']);
				$date_assoc=array(
					'day_of_the_week'=>$date[0],
					'month'=>$date[1],
					'day'=>$date[2],
					'year'=>$date[5],
					'time'=>$date[3]
				);
				$this->tweets[]=array('created_at'=>$date_assoc,'text'=>$data[$q]['text'],'source'=>$data[$q]['source']);
			}
			$this->addLinks();
			return true;
		} else {
			return false;	
		}
	}
	
	private function addLinks(){
		$a_style=' style="'.$this->get('a_style').'"';
		for($q=0;$q<sizeof($this->tweets);$q++){
			$ret=$this->tweets[$q]['text'];
			$ret = preg_replace("#(^|[\n ])([\w]+?://[\w]+[^ \"\n\r\t< ]*)#", "\\1<a href=\"\\2\" target=\"_blank\">\\2</a>", $ret);
			$ret = preg_replace("#(^|[\n ])((www|ftp)\.[^ \"\t\n\r< ]*)#", "\\1<a href=\"http://\\2\" target=\"_blank\">\\2</a>", $ret);
			$ret = preg_replace("/@(\w+)/", "<a href=\"http://www.twitter.com/\\1\" target=\"_blank\">@\\1</a>", $ret);
			$ret = preg_replace("/#(\w+)/", "<a href=\"http://search.twitter.com/search?q=\\1\" target=\"_blank\">#\\1</a>", $ret);
			$ret = str_replace('<a','<a'.$a_style,$ret);
			$this->tweets[$q]['text']=$ret;
		}
	}
	
	private function prepareJSON($input) {
		$imput = mb_convert_encoding($input, 'UTF-8', 'ASCII,UTF-8,ISO-8859-1');
		if(substr($input, 0, 3) == pack("CCC", 0xEF, 0xBB, 0xBF)) $input = substr($input, 3);
		return $input;
	}
}