<?php
include('php/config.php');
$db=new Database($config);

// This variable always outputting at the end of the doc
$output='No action called!';

if($_POST['action']){
	$action=$_POST['action'];
	
	if($action=='saveApplication'){
		unset($_POST['action']);
		$a=new Application($_POST);
		if($a->save()){
			if($_POST['application_type']=='createch'){
				$tos=array('eve.conboy@radical.ie','salvov@publicis.onmicrosoft.com');
			} else if($_POST['application_type']=='social'){
				$tos=array('emer.lawn@radical.ie');
			} else if($_POST['application_type']=='media'){
				$tos=array('sarah.mcdevitt@radical.ie','lara.quinn@radical.ie','eoin.ryan@radical.ie');
			} else if($_POST['application_type']=='search'){
				$tos=array('jess.irwin@radical.ie,davidm@publicis.onmicrosoft.com,david.mulligan@publicis.onmicrosoft.com,david.mulligan@radical.ie');
			}
			foreach($tos as $to){
				$e=new Email(array(
					'to'=>$to,
					'from'=>'info@radical.ie',
					'subject'=>'New Application on Radical.ie/jobs',
					'message'=>'<a href="http://radical.ie/jobs/get_app.php?session_code='.$_POST['session_code'].'">View application</a> or copy and paste the following link into your browser: http://radical.ie/jobs/get_app.php?session_code='.$_POST['session_code']
				));
				$e->send();
			}
			$output='Success';
		} else {
			$output='Failed';
		}
	}
}
echo $output;