<?php include('inc/functions.php');
	echo getHead("Search and Analytics Client Manager Job");
?>
	<div id="main">
		<div class="section grid_lvl1">
			<?php $breadcrumb=array(
				'All Jobs&nbsp;&nbsp;&gt;'=>'index.php',
				'Search&nbsp;&amp; Analytics Client Manager'=>'client-manager-search-analytics.php'
			); include('inc/nav.php'); ?>
			<div class="grid_lvl2">
				<main>
					<div class="grid_lvl3">
						<div class="sub_section">
							<hgroup>
								<h3 class="noborder">Search&nbsp;&amp; Analytics Client Manager</h3>
								<h5 class="margin">Ireland’s largest and fastest growing digital media agency requires a Search&nbsp;&amp; Analytics Client Manager</h5>
								<h5 class="margin">This is a great opportunity for the right person to work and learn from some of Ireland’s most experienced digital specialists and gain exposure to the world’s most exciting brands and&nbsp;projects.</h5>
							</hgroup>
						</div>
						<div class="sub_section">
							<hgroup>
								<h3>About Radical</h3>
								<h5>Radical is one of Ireland's largest and fastest growing Digital Media agencies. You’ll be part of one of our specialist teams working in Search &amp; Analytics, Digital Media Planning, Creative Technology and Social&nbsp;Media.</h5>
							</hgroup>
						</div>
						<div class="sub_section">
							<hgroup>
								<h3 class="margin">Job Spec</h3>
							</hgroup>
							<p><strong>Role &amp; Responsibilities:</strong></p>
							<ul class="bullet">				
								<li>Manage the direction and execution of Paid Search Marketing campaigns across all major engines in order to deliver outstanding Paid Search results.</li>
								<li>Manage paid search budgets and forecasting.</li>
								<li>Produce weekly, monthly and quarterly reports with actionable insights and recommendations (SEO &amp; PPC)</li>
								<li>Be instrumental in building out efficiencies in the Search Team through working with the creative technology team</li> 
								<li>Use web analytics to provide actionable and insightful recommendations for all SEM activity</li>
								<li>Plan, implements and manage SEO activity for clients</li>
								<li>Produce comprehensive SEO audits including on &amp; off page optimization</li>
								<li>Staying informed of industry trends and best practices and understanding how they can be applied to accounts</li>
							</ul>
							<p><strong>Experience:</strong></p>
							<ul class="bullet">				
								<li>2- 4 years online marketing experience with proven track record in PPC, SEO &amp; Google Analytics</li>
								<li>Experience in managing large scale, complex paid search campaigns is essential</li>
								<li>GAP qualification would be preferable</li>
								<li>Excellent knowledge of PPC optimization techniques</li>
								<li>Excellent knowledge of the search engine industry, search engine algorithms and ranking strategies</li>
								<li>Experience with 3rd party tracking (Double-click) would be preferable</li>
								<li>Experience with analytics tools in particular Google Analytics </li>
							</ul>
							<p><strong>Overall Skills:</strong></p>
							<ul class="bullet">				
								<li>Excellent working knowledge of Microsoft Office in particular</li>
								<li>Excel and PowerPoint</li>
								<li>Excellent attention to detail and the ability to multi-task in a deadline driven atmosphere</li>
								<li>Excellent written and communication skills</li>
								<li>Highly numerate and analytical</li>
							</ul>
							<p>If you have any questions about the role please contact Jess Irwin, Head of Search &amp; Analytics: jess(dot)irwin(at)radical(dot)ie</p>
						</div>
						<?php $type='blank'; include('inc/apply_form.php'); ?>
					</div>
				</main>
			</div>
		</div>
	</div>
<?php include('inc/footer.php'); ?>