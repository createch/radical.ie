<?php include('inc/functions.php');
	echo getHead("Creative Technology Project Manager Intern");
?>
	<div id="main">
		<div class="section grid_lvl1">
			<?php $breadcrumb=array(
				'All Jobs&nbsp;&nbsp;&gt;'=>'index.php',
				'Internships 2013&nbsp;&nbsp;&gt;'=>'interning-at-radical.php',
				'Project Management'=>'creative-technology-pm-intern.php'
			); include('inc/nav.php'); ?>
			<div class="grid_lvl2">
				<main>
					<div class="grid_lvl3">
						<div class="sub_section">
							<hgroup>
								<h3 class="noborder">Project Management Intern</h3>
								<h5 class="margin">Working in the Creative Technology Team, this is an opportunity to join a team of developers and designers working with some of the world's best known brands to produce digitally led creative ideas for clients.</h5>
							</hgroup>
							<p>The Creative Technology team at Radical comprises designers and developers who work to provide our clients with creative solutions to their communications challenges using digital technology.</p>
							<p>Our creative campaigns span websites, mobile apps, facebook/social media apps and advertising campaigns. We work with our colleagues in social media, search and digital media to provide integrated responses to client briefs.</p>
							<p>At the heart of our work is an understanding of client objectives, business objectives and user objectives. This informs a creative strategy and creative ideas which ensure results for our clients.</p>
						</div>
						<?php include('inc/about_radical.php'); ?>
						<div class="sub_section">
							<hgroup>
								<h3>Project Management Role</h3>
								<h5 class="margin">This is an opportunity to join a growing team as it develops at a fast pace in the coming 12 months.</h5>
							</hgroup>
							<p>You'll be meeting and managing clients, managing projects, and generally getting stuck in. It's a busy time for the team, so you'll be thrown straight into it, and learning a lot about technology, web development, UX and digital communications in the process.</p>
							<p>You will work alongside a Project Director who has over 13 years experience in creative and web design agencies in London and Dublin, and lots of knowledge and experience to share, plus a team of talented designers and developers who can teach you lots about making amazing things for the web!</p>
							<p>If you have any questions about the role please contact Eve Conboy, Head of Creative Technology: eve(dot)conboy(at)radical(dot)ie</p>
						</div>
						<div class="sub_section">
							<h3>Must haves</h3>
							<ul class="bullet">
								<li>You’ll need to have a degree in multimedia design / development / production / management, and a relevant final degree project to show us and talk through with us (or failing that, have personal projects to talk through)</li>
								<li>Ideally you will have some previous experience of managing projects in a creative studio, or worked as a production assistant/project manager etc</li>
								<li>You need to have a good basic understanding of the digital design and website front end development process from start to finish</li>
								<li>Excellent written and verbal communication skills</li>
								<li>You’ll be comfortable working at a very fast pace and juggling/multi-tasking - you don't wait to be asked!</li>
								<li>Good basic software; word, powerpoint etc</li>
								<li>You should have a strong interest in creative technology; new trends and developments in technology, interaction design and the possibilities which you’ll be happy to share with the team</li>
								<li>You’re a keen learner who is happy to take on new skills proactively</li>
							</ul>
						</div>
						<div class="sub_section">
							<h3>Nice to haves</h3>
							<ul class="bullet">
								<li>A good creative eye, happy coming up with ideas and contributing suggestions and solutions to creative challenges</li>
								<li>An understanding of the overall digital communications process including search, digital media and social media</li>
								<li>Some very basic knowledge of front end coding languages - HTML, CSS, Javascript would be an advantage</li>
							</ul>
						</div>
						<?php $type='createch'; include('inc/apply_form.php'); ?>
					</div>
				</main>
			</div>
		</div>
	</div>
<?php include('inc/footer.php'); ?>