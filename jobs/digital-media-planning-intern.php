<?php include('inc/functions.php');
	echo getHead("Digital Media Planning Intern");
?>
	<div id="main">
		<div class="section grid_lvl1">
			<?php $breadcrumb=array(
				'All Jobs&nbsp;&nbsp;&gt;'=>'index.php',
				'Internships 2013&nbsp;&nbsp;&gt;'=>'interning-at-radical.php',
				'Digital Media Planning'=>'digital-media-planning-intern.php'
			); include('inc/nav.php'); ?>
			<div class="grid_lvl2">
				<main>
					<div class="grid_lvl3">
						<div class="sub_section">
							<hgroup>
								<h3 class="noborder">Digital Media Planning Intern</h3>
								<h5 class="margin">Ireland’s largest and fastest growing digital media agency requires a Digital&nbsp;Media&nbsp;Intern.</h5>
								<h5 class="margin">This is a great opportunity for the right person to work and learn from some of Ireland’s most experienced digital specialists and gain exposure to the world’s most exciting brands and&nbsp;projects.</h5>
							</hgroup>
						</div>
						<?php include('inc/about_radical.php'); ?>
						<div class="sub_section">
							<hgroup>
								<h3>Job Spec</h3>
								<h5 class="margin">The digital media team is responsible for helping brands develop online strategies, plan activities, execute online advertising campaigns working with our creative team, traffic through adserving technology, track responses, optimising during campaigns, reporting on results and building insights to develop smarter advertising&nbsp;spends.</h5>
							</hgroup>
							<p>This exciting position will put the right person at the heart of digital advertising developments. It will also require you to work on projects across other departments within the agency including social media, search and&nbsp;creative.</p>
							<p>If you have any questions about the role please contact Sarah McDevitt, Head of Planning: sarah(dot)mcdevitt(at)radical(dot)ie</p>
						</div>
						<div class="sub_section">
							<h3>Skills</h3>
							<p>As a Digital media intern you will need to possess:</p>
							<ul class="bullet">				
								<li>A high level of skill in gathering information from both within the team and via third parties</li> 
								<li>Ability to co-ordinate and deliver projects</li>
								<li>Strong analytical and problem solving skills</li>
								<li>The ability to communicate effectively with all levels of the team</li> 
								<li>Attention to detail</li>
								<li>Ability to work as part of a team</li>
								<li>Great time management</li>
								<li>Proactive attitude</li>
								<li>The instinctual ability to get involved</li>
							</ul>
						</div>
						<?php $type='media'; include('inc/apply_form.php'); ?>
					</div>
				</main>
			</div>
		</div>
	</div>
<?php include('inc/footer.php'); ?>