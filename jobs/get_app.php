<?php
include('php/config.php');
$db=new Database($config);

$output='Whoops! Where do you think you are going?';

if(isset($_GET['session_code'])){
	$code=$_GET['session_code'];
	$apps=Query::sql('Application',array(
			'session_code'=>$code
		));
	if(sizeof($apps)>0){
		$output='<h1>Application: '.$code.'</h1>';
		$app=$apps[0];
		$vars=$app->getVars();
		foreach($vars as $var=>$val){
			$output.='<h4>'.$var.'</h4><p>'.$val.'</p>';
		}
		$dir='uploadify/uploads/'.$code;
		$files=scandir($dir);
		if(sizeof($files)>0){
			$output.='<h4>Files (CV\'s)</h4>';
			foreach($files as $file){
				if($file!='.'&&$file!='..'){
					$output.='<p><a href="http://www.radical.ie/jobs/uploadify/uploads/'.$code.'/'.$file.'" target="_blank">'.$file.'</a></p>';
				}
			}
		}
	}
}
echo '<html><head><title>Application: '.$_GET['session_code'].'</title></head><body>'.$output.'</body></html>';