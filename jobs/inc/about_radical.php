<div class="sub_section">
	<hgroup>
		<h3>About Radical</h3>
		<h5>Radical is one of Ireland's largest and fastest growing Digital Media agencies. You'll be part of one of our specialist teams working in Search &amp; Analytics, Digital Media Planning, Creative Technology and Social&nbsp;Media.</h5>
	</hgroup>
	<div class="sub_section">
		<p>Find out more about what it’s like to intern at Radical.</p>
		<ul class="list"><li><a class="button" href="interning-at-radical.php">Interning at Radical&nbsp;&gt;</a></li></ul>
	</div>
</div>