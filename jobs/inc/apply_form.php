<div class="sub_section" id="form">
	<hgroup>
		<h3>Apply</h3>
		<h5>If this sounds like you, we’d love to hear from&nbsp;you.</h5>
		<hr class="grey" />
	</hgroup>
	<div id="stage1" class="sub_section">
		<h6>Your Details</h6>
		<p class="small">We’ll need a your details so we can google you (and&nbsp;get&nbsp;in&nbsp;touch)</p>
		<input type="hidden" id="application_type" value="<?=$type;?>" />
		<div class="row input">
			<label for="name">Name</label><input type="text" name="name" id="name" value="" />
		</div>
		<div class="row input email">
			<label for="email">Email</label><input type="text" name="email" id="email" value="" />
		</div>
		<div class="row input">
			<label for="mobile">Mobile</label><input type="text" name="mobile" id="mobile" value="" />
		</div>
		<div class="row input">
			<label for="profiles">Links to your online profiles (website,&nbsp;twitter,&nbsp;linkedin,&nbsp;etc)</label><input type="text" name="profiles" id="profiles" value="" />
		</div>
	</div>
	<hr class="grey" />
	<div id="stage2" class="sub_section">
		<?php if($type=='createch'){ ?>
			<h6>Three Questions</h6>
			<p class="small">So we can get to know you a bit better</p>
			<div class="row input">
				<label for="q1">So, you've been asked to deliver the impossible - how do you&nbsp;respond?</label>
				<textarea id="q1" name="q1"></textarea>
			</div>
			<div class="row input">
				<label for="q2">What do you think the internet will be like in 5 years'&nbsp;times?</label>
				<textarea id="q2" name="q2"></textarea>
			</div>
			<div class="row input">
				<label for="q3">Tell us how you stay up to date with digital technology trends? Who do you read&nbsp;/&nbsp;follow?</label>
				<textarea id="q3" name="q3"></textarea>
			</div>
		<?php } else if($type=='createch_dev'){ ?>
			<h6>Three Questions</h6>
			<p class="small">So we can get to know you a bit better</p>
			<div class="row input">
				<label for="q1">You need to solve a bug you’ve never seen before – describe the steps you take to identify and solve the&nbsp;bug.</label>
				<textarea id="q1" name="q1"></textarea>
			</div>
			<div class="row input">
				<label for="q2">You’ve been asked to build something with a technology you are not familiar with – explain your approach to&nbsp;this.</label>
				<textarea id="q2" name="q2"></textarea>
			</div>
			<div class="row input">
				<label for="q3">List the sites/twitter feeds/blogs/magazines you like to read to stay up to date with technology, design, trends etc.</label>
				<textarea id="q3" name="q3"></textarea>
			</div>
		<?php } else if($type=='social'){ ?>
			<h6>Two Questions</h6>
			<p class="small">So we can get to know you a bit&nbsp;better</p>
			<div class="row input">
				<label for="q1">What is the most impactful social media programme / campaign / activation that you have come across in the last 12 months, and&nbsp;why?</label>
				<textarea id="q1" name="q1"></textarea>
			</div>
			<div class="row input">
				<label for="q2">What is your favourite social network, and&nbsp;why?</label>
				<textarea id="q2" name="q2"></textarea>
			</div>
		<?php } else if($type=='media'){ ?>
			<h6>Three Questions</h6>
			<p class="small">So we can get to know you a bit better</p>
			<div class="row input">
				<label for="q1">Describe your experience of using Microsoft Excel and&nbsp;PowerPoint?</label>
				<textarea id="q1" name="q1"></textarea>
			</div>
			<div class="row input">
				<label for="q2">Describe how you have used your analytic skills in the past through&nbsp;data?</label>
				<textarea id="q2" name="q2"></textarea>
			</div>
			<div class="row input">
				<label for="q3">What area of digital media interests you&nbsp;most?</label>
				<textarea id="q3" name="q3"></textarea>
			</div>
		<?php } else if($type=='search'){ ?>
			<h6>Two Questions</h6>
			<p class="small">So we can get to know you a bit&nbsp;better</p>
			<div class="row input">
				<label for="q1">Why do you want to work in search marketing?</label>
				<textarea id="q1" name="q1"></textarea>
			</div>
			<div class="row input">
				<label for="q2">How do you keep up to date with search marketing? Who do you read&nbsp;/&nbsp;follow?</label>
				<textarea id="q2" name="q2"></textarea>
			</div>
		<?php } else if($type=='blank'){ ?>
		<?php } ?>
	</div>
	<hr class="grey" />
	<div id="stage3" class="sub_section">
		<h6>Upload your CV</h6>
		<p class="small">PDF preferred, a Word doc is ok&nbsp;too</p>
		<input type="hidden" id="session_code" value="<?=rand().time();?>" />
		<input type="hidden" id="uploaded" value="false" />
		<div class="reset">
			<div id="upload_cv" class="button grey">Choose file</div>
			<input type="file" id="uploadify" />
			<p class="small upload-error">Please upload your CV.</p>
		</div>
	</div>
	<hr class="grey" />
	<div id="stage4" class="sub_section">
		<p class="small">If everything looks good, go ahead and submit your&nbsp;application!</p>
		<div id="submit" class="button">Submit</div>
	</div>
</div>