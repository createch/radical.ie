<div id="footer" class="grid_lvl1">
	<footer>
		<div class="grid_lvl2"><div class="grid_lvl3">
			<hr />
			<div class="sub_section reset">
				<ul id="social">
					<li><a target="_blank" href="https://twitter.com/radicaldublin"><div class="icon twitter"><img src="images/twitter-48.png" alt="Twitter" /></div>@radicaldublin</a><div class="clear"></div></li>
					<li><a target="_blank" href="https://www.facebook.com/RadicalDublin"><div class="icon facebook"><img src="images/facebook-48.png" alt="Facebook" /></div>facebook.com/RadicalDublin</a><div class="clear"></div></li>
					<li><a target="_blank" target="_blank" href="http://www.linkedin.com/company/1845080?trk=tyah"><div class="icon linkedin"><img src="images/linkedin-48.png" alt="LinkedIn" /></div>Radical - LinkedIn</a><div class="clear"></div></li>
				</ul>
				<a id="gptw-2" href="http://www.greatplacetowork.ie/" target="_blank"><img src="images/gptw.jpg" alt="Great place to work" /></a>
			</div>
			<div class="sub_section">
				<p id="address" ><a href="http://maps.google.ie/maps?q=16+Sir+John+Rogerson's+Quay,+Dublin&hl=en&sll=53.3834,-8.21775&sspn=9.181914,20.76416&oq=16+sir+jo&hnear=16+Sir+John+Rogerson's+Quay,+Dublin+2,+County+Dublin&t=m&z=16" target="_blank">Radical Media Ltd.<br />16 Sir John Rogersons Quay, Dublin 2</a></p>
			</div>
		</div></div>
	</footer>
</div>
</body>
</html>