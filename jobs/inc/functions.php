<?php

function getHead($title,$meta=null){
	$meta_html='';
	if($meta!=null){
		if(isset($meta['description'])){
			$meta_html.='<meta name="description" content="'.$meta['description'].'">';
		}
		if(isset($meta['keywords'])){
			$meta_html.='<meta name="keywords" content="'.$meta['keywords'].'">';
		}
	}

return '<!DOCTYPE HTML>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0"/>
<meta property="og:image" content="http://www.radical.ie/jobs/images/fb_radical.png"/>'.$meta_html.'
<title>'.$title.', Radical Digital Media</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="//use.typekit.net/aql7mio.js"></script>
<script type="text/javascript" src="uploadify/jquery.uploadifive.min.js"></script>
<script type="text/javascript" src="uploadify/jquery.uploadify.min.js"></script>
<script type="text/javascript" src="js/main.js"></script>
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push([\'_setAccount\', \'UA-37421292-1\']);
  _gaq.push([\'_trackPageview\']);
 
  (function() {
    var ga = document.createElement(\'script\'); ga.type = \'text/javascript\'; ga.async = true;
    ga.src = (\'https:\' == document.location.protocol ? \'https://\' : \'http://\') + \'stats.g.doubleclick.net/dc.js\';
    var s = document.getElementsByTagName(\'script\')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
</head>
<body>';
}