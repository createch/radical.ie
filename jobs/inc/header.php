<div id="header" class="green">
	<div class="section grid_lvl1">
		<div class="grid_lvl2">
			<header>
				<hgroup>
					<h1 id="logo" class="grid_lvl4"><img src="images/logo_white_large.png" alt="Radical" /></h1>
					<h2>We're looking for <em>awesome</em> people</h2>
				</hgroup>
				<div class="grid_lvl2">
					<hr class="thick" />
					<h6>Immediate start, online application, expenses covered.</h6>
					<h6>Plus, you’ll be first in line for a permanent role.</h6>
					<h6 id="im-awesome"><strong>I'm <em>awesome</em>, tell me more.</strong></h6>
					<div class="arrow">
						<div class="stem"></div>
						<div class="point"></div>
					</div>
				</div>
			</header>
		</div>
	</div>
</div>