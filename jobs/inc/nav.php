<div id="top">
	<hgroup>
		<h2 id="sub_logo" class="grid_lvl4"><img src="images/logo_green.png" alt="Radical" /></h2>
		<h1 class="with_logo"><a href="index.php">Jobs at Radical</a></h1>
	</hgroup>
	<hr class="medium" />
	<?php if(isset($breadcrumb)&&sizeof($breadcrumb)>0){?>
	<div id="nav-container">
		<nav>
			<ul id="breadcrumb">
				<?php $count=0; foreach($breadcrumb as $name=>$url){ ?>
				<li id="breadcrumb_lvl<?=$count;?>"><a href="<?=$url;?>"><?=$name;?></a></li>
				<?php $count++; } ?>
			</ul>
			<div class="clear"></div>
			<hr class="notop" />
		</nav>
	</div>
	<?php } ?>
</div>