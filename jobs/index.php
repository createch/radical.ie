<?php 

header("Location: https://onjobbio.com/company/35/profile/0");
return; 

include('inc/functions.php');
	echo getHead("We're looking for awesome people"); ?>
	<div id="main">
		<div class="section grid_lvl1">
			<?php include('inc/nav.php'); ?>
			<div class="grid_lvl2">
				<main>
					<?php include('inc/current_positions.php'); ?>
					<div class="grid_lvl3">
						<div class="sub_section">
							<hgroup>
								<h3>Working at Radical</h3>
								<h5>This is what you need to know about working at Radical.</h5>
							</hgroup>
						</div>
						<div class="sub_section">
							<h6><strong>Radical is one of Ireland's largest and fastest growing Digital&nbsp;Media&nbsp;agencies</strong></h6>
							<p>We have four specialist teams working in Search &amp; Analytics, Digital Media Planning, Creative Technology and Social Media. Radical is part of <a href="http://www.coremedia.ie" target="_blank">Core Media</a>, Ireland’s largest media buying&nbsp;group.</p>
							<h6><strong>This is a great place to work</strong></h6>
							<p>
								<a href="http://www.greatplacetowork.ie/" target="_blank"><img src="images/gptw.jpg" alt="Great place to work" id="gptw-1" /></a>
								No, seriously, we have been awarded the <a href="http://www.greatplacetowork.ie/" target="_blank">Great Place to Work</a> award for the past 3 years running. View our ranking on greatplacetowork.ie. You'll be working in our bright, brand new offices which overlook the River Liffey and Samuel Beckett bridge in the heart&nbsp;of&nbsp;Dublin.
							</p>
							<h6><strong>Radical is a place for you to grow and&nbsp;develop</strong></h6>
							<p>We recognise emerging talent as the future of creativity and advertising and you’ll be an integral part of our&nbsp;team.</p>
						</div>
						<div class="sub_section">
							<div class="sub_section">
								<hgroup>
									<h3>Apply now!</h3>
									<h5>View details of our current opportunities and submit your application online. We can't wait to hear from&nbsp;you!</h5>
								</hgroup>
							</div>
							<div class="sub_section">
								<?php $just_links=true; include('inc/current_positions.php'); ?>
							</div>
							<div class="addthisnow sub_section">
								
							</div>
						</div>
					</div>
				</main>
			</div>
		</div>
	</div>
<?php include('inc/footer.php'); ?>