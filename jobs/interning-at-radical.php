<?php include('inc/functions.php');
	echo getHead("Interning at Radical");
?>
	<div id="main">
		<div class="section grid_lvl1">
			<?php $breadcrumb=array(
				'All Jobs&nbsp;&nbsp;&gt;'=>'index.php',
				'Internships 2013'=>'interning-at-radical.php'
			);
			include('inc/nav.php'); ?>
			<div class="grid_lvl2">
				<main>
					<?php include('inc/current_internships.php'); ?>
					<div class="grid_lvl3">
						<div class="sub_section">
							<hgroup>
								<h3>Interning at Radical</h3>
								<h5>Here's what you need to know about interning at&nbsp;Radical.</h5>
							</hgroup>
						</div>
						<div class="sub_section">
							<h6><strong>Radical is one of Ireland's largest and fastest growing Digital&nbsp;Media&nbsp;agencies.</strong></h6>
							<p>We have four specialist teams working in Search &amp; Analytics, Digital Media Planning, Creative Technology and Social Media. Radical is part of <a href="http://www.coremedia.ie" target="_blank">Core Media</a>, Ireland’s largest media buying&nbsp;group.</p>
							<h6><strong>As an intern you'll do real work and gain real hands on&nbsp;experience</strong></h6>
							<p>We work with some of the world's best known brands and you’ll get the opportunity to work on high profile campaigns. Your days will definitely not be spent making tea and browsing Facebook (except for our social media interns — they're always on&nbsp;Facebook...).</p>
							<h6><strong>This is a fantastic opportunity to kick start your&nbsp;career</strong></h6>
							<p>All of our previous interns have gone on to accept full time paid positions working at Radical and across the Core Media Group, so this is a smart move for your&nbsp;career.</p>
							<h6><strong>This is a great place to&nbsp;work</strong></h6>
							<p>No, seriously, we have been awarded the Great Place to Work award for the past 3 years running. View our ranking on greatplacetowork.ie. You'll be working in our bright, brand new offices which overlook the River Liffey and Samuel Beckett bridge in the heart of&nbsp;Dublin.</p>
							<h6><strong>Radical is a place for you to grow and&nbsp;develop</strong></h6>
							<p>We recognise emerging talent as the future of creativity and advertising and you’ll be an integral part of our&nbsp;team.</p>
							<p>And, as a potential intern, you’ll be interested to know that 100% of our employees fully agreed with the statement: "I'm treated as a full member regardless of my&nbsp;position." </p>
						</div>
					</div>
					<div id="perks" class="grid_lvl3 green">
						<div class="padding">
							<div class="sub_section">
								<hgroup>
									<h3 class="noborder">We don’t do working&nbsp;for&nbsp;free</h3>
								</hgroup>
							</div>
							<div class="sub_section">
								<div class="">
									<div class="left_image"><img src="images/icon_busticket.png" alt="Bus ticket icon" /></div>
									<div class="right_content">
										<h6><strong>Lunch and transport is on us</strong></h6>
										<p>We'll pay for your transport to and from the office and we'll give you a lunch allowance each day. There should be a bit left over for beer money&nbsp;too.</p>
									</div>
									<div class="clear"></div>
								</div>
								<div class="">
									<div class="clear"></div>
									<div class="left_image"><img src="images/icon_laptop.png" alt="Laptop icon" /></div>
									<div class="right_content">
										<h6><strong>Your own PC laptop or mac</strong></h6>
										<p>You’ll get your own PC laptop or mac to use for the duration of your&nbsp;internship.</p>
									</div>
									<div class="clear"></div>
								</div>
								<div class="">
									<div class="left_image"><img src="images/icon_yoda.png" alt="Yoda icon" /></div>
									<div class="right_content">
										<h6><strong>Weekly mentoring</strong></h6>
										<p>Each week, there will be time allocated for mentoring with senior management. It’s a chance for you to ask lots of questions and get valuable&nbsp;advice.</p>
									</div>
									<div class="clear"></div>
								</div>
							</div>
						</div>
					</div>
					<div class="grid_lvl3">
						<div class="sub_section">
							<hgroup>
								<h3>Our Previous Interns</h3>
								<h5>We've offered full-time roles to all of our interns in the past 12 months, plus various other team members over the&nbsp;years.</h5>
							</hgroup>
						</div>
						<div class="sub_section">
							<ul id="previous-interns">
								<li class="sub_section">
									<h5 class="name">Naomi MacHale</h5>
									<p><strong>Digital Media Client Associate</strong></p>
									<div class="picture"><img src="images/naomi.jpg" alt="Picture of Naomi MacHale" /></div>
									<div class="quote"><h5><em>"On top of it all it's a fun place to&nbsp;work!"</em></h5></div>
									<div class="content">
										<p>I was fortunate enough to progress from intern to a full time role as Digital Client Associate at Radical. I now work across many leading advertisers supporting a larger digital media team. Radical allows me to grow, learn, and progress within this dynamic, exciting and steadily growing industry. I am fortunate enough to be continuously learning and developing and on top of it all it’s a fun place to&nbsp;work!</p>
									</div>
									<div class="clear"></div>
								</li>
								<li class="sub_section">
									<h5 class="name">Amanda Weir</h5>
									<p><strong>Digital Media Client Associate</strong></p>
									<div class="picture"><img src="images/amanda.jpg" alt="Picture of Amanda Weir" /></div>
									<div class="quote"><h5><em>"Serious time was invested in training and I was doing real work for&nbsp;clients."</em></h5></div>
									<div class="content">
										<p>While many associate the term 'intern' with making coffee and photocopying, this is absolutely not the case in Radical. From day 1, serious time was invested in training and I was doing real work for clients. I was really looked after during my internship and was straight away made feel part of the team by this crazy bunch of talented&nbsp;people.</p>
									</div>
									<div class="clear"></div>
								</li>
								<li class="sub_section">
									<h5 class="name">Laura McCarthy</h5>
									<p><strong>Digital Media Client Associate</strong></p>
									<div class="picture"><img src="images/laura.jpg" alt="Picture of Laura McCarthy" /></div>
									<div class="quote"><h5><em>"I worked across teams as well such as search, social and creative&nbsp;development."</em></h5></div>
									<div class="content">
										<p>The internship was honestly brilliant. From day one I was involved in everything from training on industry tools to being showed all the elements of planning. In terms of the company, everyone was very welcoming. (Social life was great!) I worked across teams as well such as search, social and creative development. I was delighted then when I got the full time role, especially since it's a great company and I'm gaining valuable knowledge in a growing area. Overall the internship was a great chance to get a feel for what the company &amp; job role would be like. It also made it so much easier that I had a very nice, understanding &amp; helpful&nbsp;manager! </p>
									</div>
									<div class="clear"></div>
								</li>
								<li class="sub_section">
									<h5 class="name">David Mulligan</h5>
									<p><strong>Search and Analytics Client Director</strong></p>
									<div class="picture"><img src="images/david.jpg" alt="Picture of David Mulligan" /></div>
									<div class="quote"><h5><em>"I have represented the company at conferences and in places I never imagined&nbsp;visiting"</em></h5></div>
									<div class="content">
										<p>I began working in Radical 3 years ago as an intern on the Search Marketing team. A friend of mine had just completed his internship here and was asked to stay on so I knew if I showed potential there would be a paid job at the end of the 3 months. Since joining full time I have not been disappointed one bit, no 2 days are the same. Over the years I have learnt so much, gained training across all areas of media, represented the company at conferences at places I never imagined visiting and have been rewarded for the work and effort I put in along the&nbsp;way.</p>
									</div>
									<div class="clear"></div>
								</li>
							</ul>
						</div>
					</div>
					<div class="grid_lvl3">
						<div class="sub_section">
							<hgroup>
								<h3>Apply now!</h3>
								<h5>View details of our current internships and submit your application online. We can't wait to hear from you!</h5>
							</hgroup>
						</div>
						<div class="sub_section">
							<?php $just_links=true; include('inc/current_internships.php'); ?>
						</div>
						<div class="addthisnow sub_section">
						</div>
					</div>
				</main>
			</div>
		</div>
	</div>
<?php include('inc/footer.php'); ?>