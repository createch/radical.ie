//load TypeKit
try{Typekit.load();}catch(e){}

$(document).ready(function(){
	//breakpoints();
	initUploadifive();
	$('#submit').click(function(){
		if(validate($('#stage1'))){
			if(validate($('#stage2'))){
				if($('#uploaded').val()=='true'){
					saveApplication();
				} else {
					$('#stage3 .upload-error').show();
				}
			}
		}
	});

	$('.addthisnow').each(function(){
		$(this).append('<div class="addthis_toolbox addthis_default_style "><a class="addthis_button_facebook_like" fb:like:layout="button_count"></a><a class="addthis_button_tweet"></a><a class="addthis_button_pinterest_pinit"></a><a class="addthis_counter addthis_pill_style"></a></div><script type="text/javascript">var addthis_config = {"data_track_addressbar":false};</script><script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-4f914ebb149eca60"></script>');
	});
});

function saveApplication(){
	$.post('ajax.php',{
		action:'saveApplication',
		session_code:$('#session_code').val(),
		application_type:$('#application_type').val(),
		name:$('#name').val(),
		email:$('#email').val(),
		mobile:$('#mobile').val(),
		profiles:$('#profiles').val(),
		q1:$('#q1').val(),
		q2:$('#q2').val(),
		q3:$('#q3').val(),
		uploaded:$('#uploaded').val()
	},function(r){
		if(r=='Success'){
			$('#submit').replaceWith('<p><strong>Thank you for applying</strong></p><p>We will get back to you once we have reviewed your application!</p>');
		} else {
			alert('An error has occurred... Please refresh the page and try again.');
		}
	});
			
}

function breakpoints(){
	$('body').append('<div id="breakpoint"></div>');
	$('#breakpoint').css({background: '#000',color: '#fff',position: 'fixed',bottom: '0px',right: '0px',padding: '10px'});
	$(window).resize(function(){$('#breakpoint').text($(this).width()+' : '+$(this).height());});
	$('#submit').click(function(){
		if(validate($('#stage1'))){
			if(validate($('#stage2'))){
				alert('done');
			}
		}
	});
}

function validate(e){
	var valid=true;
	e.find('.input').each(function(){
		if($(this).find('input,textarea').length>0){
			var isEmail=false;
			if($(this).hasClass('email')){
				isEmail=true;
			}
			var s=$(this).find('input,textarea');
			if(s.val()==''||s.val()==s.attr('rel')||(isEmail&&!isValidEmailAddress(s.val()))){
				s.parent().parent().removeClass('valid');
				s.parent().parent().addClass('invalid');
				valid=false;
			} else {
				s.parent().parent().removeClass('invalid');
				s.parent().parent().addClass('valid');
			}
		}
	});
	if(!valid){ scrollTo(e); }
	return valid;
}

function scrollTo(e){
	var header_height=0;
	$('html,body').animate({
         scrollTop: e.offset().top-header_height
    }, 500);
}

// UPLOADER
function initUploadifive(){
	$('#uploadify').uploadifive({
		'buttonText' : '',
        'uploadScript' : 'uploadify/uploadify.php?session_code='+$('#session_code').val(),
        'fileSizeLimit' : '5MB',
        'auto'         : true,
        'height'   : '58px',
		'width'	: '100%',
        'onFallback'   : function() {
        	initUploadify();
        },
        'onUploadComplete' : function(file, data) {
			if(data=='Success'){
				$('#uploaded').val('true')
				$('#stage3 .reset').replaceWith('<p><em>Your CV has been uploaded!</em></p>');
			} else {
				alert(data);
			}
        },
        'onInit':function(instance){
        	$('#uploadifive-uploadify').attr('style','overflow: hidden;opacity: 0;position: absolute;top: 0;left: 0;width: 100%;height: 58px;cursor:pointer;z-index:10;');
        }
    }); 
}

function initUploadify(){
	$('#uploadify').uploadify({
		'buttonText' : '',                                
		'wmode'    : 'transparent',
		'swf'      : 'uploadify/uploadify.swf',
		'uploader' : 'uploadify/uploadify.php?session_code='+$('#session_code').val(),
		'fileSizeLimit' : '5MB',
		'fileTypeExts' : '*.doc; *.docx; *.DOC; *.DOCX; *.PDF; *.pdf;',
		'preventCaching' : false,
		'folder'    : 'uploadify/uploads',
      	'auto'      : true,
      	'height'   : '58px',
		'width'	: '100%',
		'onUploadSuccess' : function(file, data, response) {
			if(data=='Success'){
				$('#uploaded').val('true');
				$('#stage3 .reset').replaceWith('<p><em>Your CV has been uploaded!</em></p>');
			} else {
				alert(data);
			}
		},
		'onInit':function(instance){
			$('#uploadify.uploadify').attr('style','overflow: hidden;opacity: 0;position: absolute;top: 0;left: 0;width: 100%;height: 58px;cursor:pointer;z-index:10;');
		}
	});
}

function isValidEmailAddress(emailAddress) {
	var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
	return pattern.test(emailAddress);
}