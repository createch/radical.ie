<?php include('inc/functions.php');
	echo getHead("Junior Developer,",array(
			'description'=>'Junior Developer Role in Dublin at Radical Digital Media Agency which would suit a recent graduate from a Computer Science or equivalent degree.'));
?>
	<div id="main">
		<div class="section grid_lvl1">
			<?php $breadcrumb=array(
				'All Jobs&nbsp;&nbsp;&gt;'=>'index.php',
				'Junior Developer'=>'junior-developer.php'
			); include('inc/nav.php'); ?>
			<div class="grid_lvl2">
				<main>
					<div class="grid_lvl3">
						<div class="sub_section">
							<hgroup>
								<h3 class="noborder">Junior Developer</h3>
								<h5 class="margin">Working in the Creative Technology Team, this is an opportunity to join a team of developers and designers working with some of the world's best known brands to produce digitally led creative ideas for clients.</h5>
							</hgroup>
						</div>
						<div class="sub_section">
							<hgroup>
								<h3>About Radical</h3>
								<h5>Radical is one of Ireland's largest and fastest growing Digital agencies. You'll be part of one of our specialist teams working in Search &amp; Analytics, Digital Media Planning, Creative Technology and Social&nbsp;Media.</h5>
							</hgroup>
						</div>
						<div class="sub_section">
							<hgroup>
								<h3>About the role</h3>
							</hgroup>
							<p>We're looking for a smart, creative, curious and ambitious junior developer to join the Creative Technology team.</p>
							<p>This is a junior position, so would ideally suit a recent graduate from a Computer Science or equivalent degree.</p>
							<p>The Creative Technology team at Radical comprises designers and developers who work to provide our clients with creative solutions to their communications challenges using technology.</p>
							<p>Our creative campaigns span websites, mobile apps, facebook/social media apps and advertising campaigns. We work with our colleagues in social media, search and digital media to provide integrated responses to client briefs.</p>
							<p>At the heart of our work is an understanding of client objectives, business objectives and user objectives. This informs a creative strategy and creative ideas which ensure results for our clients.</p>
						</div>
						<div class="sub_section">
							<h3>Must haves:</h3>
							<ul class="bullet">
								<li>Solid, standards based knowledge of HTML and CSS. (inc. HTML5/CSS3 and cross browser compatibility)</li>
								<li>Demonstrable understanding of javascript and Jquery</li>
								<li>Good Knowledge of object-oriented PHP</li>
								<li>Basic understanding of LAMP environment</li>
								<li>Experience of working with at least one of the leading open-source CMS’s (eg Wordpress, Drupal, Joomla)</li>
								<li>Basic Knowledge of Photoshop, Illustrator and/or Indesign</li>
								<li>Basic usage of Flash and ActionScript 3</li>
								<li>Strong communication skills</li>
								<li>Comfortable working in a creative team, contributing ideas, solving problems and sharing knowledge</li>
								<li>Interested in design, communications and how technology is shaping and changing these areas</li>
							</ul>
						</div>
						<div class="sub_section">
							<h3>Nice to haves:</h3>
							<ul class="bullet">
								<li>CSS preprocessor (Sass, LESS)</li>
								<li>PHP frameworks (Zend, Laravel, CakePHP)</li>
								<li>HTML Canvas</li>
								<li>API's (Oauth – Facebook, Twitter, Google)</li>
							</ul>
						</div>
						<?php $type='createch_dev'; include('inc/apply_form.php'); ?>
					</div>
				</main>
			</div>
		</div>
	</div>
<?php include('inc/footer.php'); ?>