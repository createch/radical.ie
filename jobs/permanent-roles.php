<?php include('inc/functions.php');
	echo getHead("Permanent roles at Radical");
?>
	<div id="main">
		<div class="section grid_lvl1">
			<?php include('inc/nav.php'); ?>
			<div class="grid_lvl2">
				<main>
					<?php include('inc/current_positions.php'); ?>
					<div class="grid_lvl3">
						<hr />
						<div class="sub_section">
							<hgroup>
								<h3>Interning at Radical</h3>
								<h5>Here's what you need to know about interning at Radical.</h5>
							</hgroup>
						</div>
						<div class="sub_section">
							<h6><strong>Radical is one of Ireland's largest and fastest growing Digital Media agencies.</strong></h6>
							<p>We have four specialist teams working in Search &amp; Analytics, Digital Media Planning, Creative Technology and Social Media. Radical is part of  ia, one of the Ireland’s largets media buying groups.</p>
							<h6><strong>As an intern you'll do real work and gain real hands on experience</strong></h6>
							<p>We work with some of the World's best known brands and you’ll get the opportunity to work on high profile campaigns. Your days will definitely not be spent around making tea and browsing Facebook (except for our social media interns — they're always on Facebook...).</p>
							<h6><strong>This is a fantastic opportunity to kick start your career</strong></h6>
							<p>All of our previous interns have gone on to accept full time paid positions working at Radical and across the Core Media Group, so this is a smart move for your career.</p>
							<h6><strong>This is a great place to work</strong></h6>
							<p>No, seriously, we have been awarded the Great Place to Work award for the past 3 years running. View our ranking on greatplacetowork.ie. You'll be working in our bright, brand new offices which overlook the River Liffey and Samuel Beckett bridge in the heart of Dublin.</p>
							<h6><strong>Radical is place for you to grow and develop</strong></h6>
							<p>We recognise emerging talent as the future of creativity and advertising and you’ll be an integral part of our team.</p>
							<p>And, as a potential intern, you’ll be interested to know that 100% of our employees fully agreed with the statement: "I'm treated as a full member regardless of my position." </p>
						</div>
					</div>
					<div class="grid_lvl3 green">
						<div class="padding">
							<div class="sub_section">
								<hgroup>
									<h3>We don’t do working for free</h3>
									<h5>Rationale behind interning here...</h5>
								</hgroup>
							</div>
							<div class="sub_section">
								<div class="sub_section">
									<div class="left_image"><img src="images/icon_busticket.png" alt="Bus ticket icon" /></div>
									<div class="right_content">
										<h6><strong>Lunch and transport is on us</strong></h6>
										<p>We'll pay for your transport to and from the office and we'll give you a lunch allowance each day. There should be a bit left over for beer money too.</p>
									</div>
									<div class="clear"></div>
								</div>
								<div class="sub_section">
									<div class="clear"></div>
									<div class="left_image"><img src="images/icon_laptop.png" alt="Laptop icon" /></div>
									<div class="right_content">
										<h6><strong>Your own PC laptop or mac</strong></h6>
										<p>You’ll get your own PC laptop or mac to use for the duration of your internship.</p>
									</div>
									<div class="clear"></div>
								</div>
								<div class="sub_section">
									<div class="left_image"><img src="images/icon_yoda.png" alt="Yoda icon" /></div>
									<div class="right_content">
										<h6><strong>Weekly mentoring</strong></h6>
										<p>Each week, there will be time allocated for mentoring with senior management. It’s a chance for you to ask lots of questions and get valuable advice.</p>
									</div>
									<div class="clear"></div>
								</div>
							</div>
						</div>
					</div>
					<div class="grid_lvl3">
						<hr />
						<div class="sub_section">
							<hgroup>
								<h3>Our previous interns</h3>
								<h5>We've offered full-time roles to four of our interns in the past 12 months, plus various other team members over the years.</h5>
							</hgroup>
						</div>
						<div class="sub_section">
							<ul id="previous-interns">
								<li class="sub_section">
									<h5 class="name">Naomi MacHale</h5>
									<p><strong>Digital Media Client Associate</strong></p>
									<div class="picture"><img src="images/naomi.jpg" alt="Picture of Naomi MacHale" /></div>
									<div class="quote"><h5><em>"On top of it all it's a fun place to work!"</em></h5></div>
									<div class="content">
										<p>I was fortunate enough to progress from intern to a full time role as Digital Client Associate at Radical. I now work across many leading advertisers supporting a larger digital media team. Radical allows me to grow, learn, and progress within this dynamic, exciting and steadily growing Industry. I am fortunate enough to be continuously ascertaining expertise leadership and knowledge while developing both personally and professionally and on top of it all it’s a fun place to work!</p>
									</div>
									<div class="clear"></div>
								</li>
								<li class="sub_section">
									<h5 class="name">Amanda Weir</h5>
									<p><strong>Digital Media Client Associates</strong></p>
									<div class="picture"><img src="images/amanda.jpg" alt="Picture of Amanda Weir" /></div>
									<div class="quote"><h5><em>"Serious time was invested in training and I was doing real work for clients."</em></h5></div>
									<div class="content">
										<p>While many associate the term 'intern' with making coffee and photocopying, this is absolutely not the case in Radical. From day 1, serious time was invested in training and I was doing real work for clients. I was very much so looked after during my internship and was straight away made feel part of the team by this crazy bunch of talented people.</p>
									</div>
									<div class="clear"></div>
								</li>
								<li class="sub_section">
									<h5 class="name">Laura McCarthy</h5>
									<p><strong>Digital Media Client Associate</strong></p>
									<div class="picture"><img src="images/laura.jpg" alt="Picture of Laura McCarthy" /></div>
									<div class="quote"><h5><em>"There is a great social aspect and brilliant prospects for career progression."</em></h5></div>
									<div class="content">
										<p>The internship was honestly brilliant. From day one I was involved in everything from training on industry tools to being showed all the elements of planning. In terms of the company, everyone was very welcoming. (Social life was great!) I worked across teams as well such as search, social and creative development. I was delighted then when I got the full time role, especially since it's a great company and I'm gaining valuable knowledge in a growing area. Overall the internship was a great chance to get a feel for what the company &amp; job role would be like. It also made it so much easier that I had a very nice, understanding &amp; helpful manager! </p>
									</div>
									<div class="clear"></div>
								</li>
								<li class="sub_section">
									<h5 class="name">Fiona McGrath</h5>
									<p><strong>Search and Analytics Client Associate</strong></p>
									<div class="picture"><img src="images/fiona.jpg" alt="Picture of Fiona McGrath" /></div>
									<div class="quote"><h5><em>"There is a great social aspect and brilliant prospects for career progression."</em></h5></div>
									<div class="content">
										<p>I began as an offline intern in Mediavest learning about strategic planning, targeting and audience segmentation. However during my time there I realised digital marketing was the direction I wanted to go in my career. It was then recommended that I transferred to another internship on the search team in radical. I was accepted and in time became a permanent member of the team. I have been working within the Core group for over a year now and am really enjoying it. There is a great social aspect and brilliant prospects for career progression.</p>
									</div>
									<div class="clear"></div>
								</li>
								<li class="sub_section">
									<h5 class="name">David Mulligan</h5>
									<p><strong>Search and Analytics Senior Client Manager</strong></p>
									<div class="picture"><img src="images/david.jpg" alt="Picture of David Mulligan" /></div>
									<div class="quote"><h5><em>"I have represented the company at conferences and in places I never imagined visiting"</em></h5></div>
									<div class="content">
										<p>I began working in Radical 3 years ago as an intern on the Search Marketing team. A friend of mine had just completed his internship here and was asked to stay on so I knew if I showed potential there would be a paid job at the end of the 3 months. Since joining full time I have not been disappointed one bit, no 2 days are the same. Over the years I have learnt so much, gained training across all areas of media, represented the company at conferences at places I never imagined visiting and have been awarded for the work and effort I put in along the way.</p>
									</div>
									<div class="clear"></div>
								</li>
							</ul>
						</div>
					</div>
					<div class="grid_lvl3">
						<hr />
						<div class="sub_section">
							<hgroup>
								<h3>Apply now!</h3>
								<h5>View details of our current internships and submit your application online. We can't wait to hear from you!</h5>
							</hgroup>
						</div>
						<div class="sub_section">
							<ul class="list">
								<li><a class="button" href="creative-technology-pm-intern.php">Creative Technology Project Management Intern&nbsp;&gt;</a></li>
								<li><a class="button" href="social-and-community-intern.php">Social &amp; Community Intern&nbsp;&gt;</a></li>
								<li><a class="button" href="digital-media-planning-intern.php">Digital Media Planning Intern&nbsp;&gt;</a></li>
							</ul>
						</div>
						<div class="sub_section">
							<div class="addthis_toolbox addthis_default_style ">
							<a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
							<a class="addthis_button_tweet"></a>
							<a class="addthis_button_pinterest_pinit"></a>
							<a class="addthis_counter addthis_pill_style"></a>
							</div>
							<script type="text/javascript">var addthis_config = {"data_track_addressbar":false};</script>
							<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-4f914ebb149eca60"></script>
						</div>
					</div>
				</main>
			</div>
		</div>
	</div>
<?php include('inc/footer.php'); ?>