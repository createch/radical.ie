<?php
	class Database {
		
		private $conn,$db;
		private $type='Server';
		
		public function __construct($config){
			$this->conn=new Connection($config['server'],$config['user'],$config['pass']);
			$this->setDatabase($config['db']);
		}
		
		public function setLocal($vars){
			if(!$this->conn->connected()){
				$this->conn=new Connection($vars['server'],$vars['user'],$vars['pass']);
				if($this->conn->connected()){
					$this->type='Local';	
				}
			}
			if($db){
				$this->setDatabase($db);	
			}
		}
		
		private function setDatabase($new_db){
			$this->db=$new_db;
			if(mysql_select_db($new_db)){
				return true;
			} else {
				return false;
			}
		}
		
		public function getConnectionStatus(){
			return $this->type.': '.$this->conn->getStatus();
		}
	}
?>