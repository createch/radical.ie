<?php
	class Email extends Entry {
		
		public function __construct($vars=NULL){
			parent::__construct($vars);
			$this->set(array('delimiter'=>'::'));	
		}
		
		private function emailReady(){
			if($this->get('to')&&$this->get('from')&&$this->get('subject')&&$this->get('message')){
				return true;
			} else {
				return false;	
			}
		}
		
		public function send(){
			if($this->emailReady()){
				$headers .= "MIME-Version: 1.0\r\n";
				$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
				$headers .= "From: ".$this->get('from')."\r\n";
				if(mail($this->get('to'), $this->get('subject'), $this->get('message'), $headers)){
					return true;	
				} else {
					return false;	
				}
			}
		}
		
		public function loadHTML($file){
			$finalHTML='';
			$html=file_get_contents('html/'.$file);
			$sections=explode($this->get('delimiter'),$html);
			for($q=0;$q<sizeof($sections);$q++){
				if(($q%2)==1){
					$sections[$q]=$this->get($sections[$q]);
				}
				$finalHTML.=$sections[$q];
			}
			$this->set(array('message'=>$finalHTML));
		}
	}