<?php include('inc/functions.php');
	echo getHead("Search &amp; Analytics Intern",array(
			'description'=>'We are looking for a great Intern to join our team and assist in day-to-day tasks related to PPC, search engine optimisation (SEO), and Web Analytics.'
		));
?>
	<div id="main">
		<div class="section grid_lvl1">
			<?php $breadcrumb=array(
				'All Jobs&nbsp;&nbsp;&gt;'=>'index.php',
				'Internships 2013&nbsp;&nbsp;&gt;'=>'interning-at-radical.php',
				'Search &amp; Analytics Intern'=>'search-analytics-intern.php'
			); include('inc/nav.php'); ?>
			<div class="grid_lvl2">
				<main>
					<div class="grid_lvl3">
						<div class="sub_section">
							<hgroup>
								<h3 class="noborder">Search &amp; Analytics Intern</h3>
								<h5 class="margin">This is a great opportunity for the right person to work and learn from some of Ireland’s most experienced digital specialists and gain exposure to the world’s most exciting brands and&nbsp;projects.</h5>
							</hgroup>
							<p>We are looking for a great Intern to join our team and assist in day-to-day tasks related to PPC, search engine optimisation (SEO), and Web Analytics.  This position requires an innovative, dependable self-starter with a basic understanding and passion for PPC, SEO and online analytics&nbsp;tools.</p>
						</div>
						<?php include('inc/about_radical.php'); ?>
						<div class="sub_section">
							<hgroup>
								<h3 class="margin">Job Spec</h3>
							</hgroup>
							<p><strong>Position Requirements / Responsibilities:</strong></p>
							<ul class="bullet">				
								<li>Learn about PPC strategies, campaign management and tools</li>
								<li>Assist in creating and optimising PPC campaigns</li>
								<li>Perform keyword research</li>
								<li>Write PPC ad copy</li>
								<li>Manage bids and keyword positions to achieve desired Return on Investment (ROI)</li>
								<li>Perform weekly, monthly and ad hoc reporting including PPC analysis and recommendations</li>
								<li>Monitor website performance, trends, and campaigns using Google Analytics</li>
								<li>Assist in conducting SEO audits &amp; performance reports</li>
								<li>Keep pace with PPC, SEO and Web Analytics industry trends and developments</li>
							</ul>
						</div>
						<div class="sub_section">
							<hgroup>
								<h3>Qualifications</h3>
							</hgroup>
							<ul class="bullet">				
								<li>Strong computer skills, especially Microsoft Excel, Word &amp; PowerPoint</li>
								<li>Strong analytical and problem solving skills</li>
								<li>Excellent written and verbal communication skills</li>
								<li>Excellent time-management and organisational skills</li>
								<li>Ability to quickly learn new technology platforms</li>
								<li>Ability to work well with others in a team</li>
								<li>Self-motivated and ability to multi-task</li>
								<li>A sharp eye for detail with the ability to see how details fit into the 'BIG' picture.</li>
							</ul>
						</div>
						<div class="sub_section">
							<hgroup>
								<h3>What we'll give you in return</h3>
							</hgroup>
							<ul class="bullet">				
								<li>Training and development at one of Ireland’s leading digital marketing&nbsp;agencies</li>
								<li>Unrivalled career progression</li>
								<li>An inspiring &amp; social team to work with</li>
								<li>Excellent communication and presentation skills</li>
								<li>Excellent analytical, written and numerical skills</li>
								<li>Excellent organisational skills</li>
								<li>Able to prioritise, sometimes in a pressurised&nbsp;environment</li>
								<li>Experience of MS Excel, Word &amp; PowerPoint</li>	
							</ul>
							<p>If this sounds like you then fill out the form below.</p>
							<p>If you have any questions about the role please contact David Mulligan: david(dot)mulligan(at)radical(dot)ie</p>
						</div>
						<?php $type='search'; include('inc/apply_form.php'); ?>
					</div>
				</main>
			</div>
		</div>
	</div>
<?php include('inc/footer.php'); ?>