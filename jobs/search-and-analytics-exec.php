<?php include('inc/functions.php');
	echo getHead("Search and Analytics Marketing Executive Job",array(
			'description'=>'PPC, SEO and Analytics Role in Dublin at Radical Digital Media Agency with 1 Years Search Marketing and Website Analytics Job Experience.'
		));
?>
	<div id="main">
		<div class="section grid_lvl1">
			<?php $breadcrumb=array(
				'All Jobs&nbsp;&nbsp;&gt;'=>'index.php',
				'Search &amp; Analytics Executive'=>'search-and-analytics-exec.php'
			); include('inc/nav.php'); ?>
			<div class="grid_lvl2">
				<main>
					<div class="grid_lvl3">
						<div class="sub_section">
							<hgroup>
								<h3 class="noborder">Search &amp; Analytics Executive</h3>
								<h5 class="margin">Ireland’s largest and fastest growing digital media agency requires a Search &amp; Analytics&nbsp;Executive.</h5>
								<h5 class="margin">This is a great opportunity for the right person to work and learn from some of Ireland’s most experienced digital specialists and gain exposure to the world’s most exciting brands and&nbsp;projects.</h5>
							</hgroup>
						</div>
						<div class="sub_section">
							<hgroup>
								<h3>About Radical</h3>
								<h5>Radical is one of Ireland's largest and fastest growing Digital Media agencies. You’ll be part of one of our specialist teams working in Search &amp; Analytics, Digital Media Planning, Creative Technology and Social&nbsp;Media.</h5>
							</hgroup>
						</div>
						<div class="sub_section">
							<hgroup>
								<h3 class="margin">Job Spec</h3>
							</hgroup>
							<p><strong>What we'll want you to do</strong></p>
							<ul class="bullet">				
								<li>Assist the Search &amp; Analytics Manager with daily tasks related to the optimisation of PPC accounts including launching and restructuring campaigns, expanding keywords lists, trafficking , ad copy creation testing&nbsp;&amp;&nbsp;Billing</li>
								<li>Regular &amp; Ad hoc performance reporting including interpretation of data and making recommendations about&nbsp;improvements.</li>
								<li>Manage daily spend in line with targets and monthly&nbsp;budgets</li>
								<li>Assist the Search &amp; Analytics Manager to create and execute SEO strategies for client sites with the goal of increasing search engine organic traffic and&nbsp;leads</li> 
								<li>Assist the Search &amp; Analytics Manager to plan and implement link building&nbsp;campaigns</li>
							</ul>
						</div>
						<div class="sub_section">
							<hgroup>
								<h3>The details</h3>
							</hgroup>
							<p><strong>What you'll need to demonstrate</strong></p>
							<ul class="bullet">				
								<li>You will have a minimum of 1 years SEM experience&nbsp;(Both SEO/PPC)</li>
								<li>An understanding of industry tools to research keywords, manage and optimise PPC&nbsp;campaigns.</li>
								<li>A confident and demonstrable knowledge of search engine ranking factors both on-page and off-page including industry tools, tactics and algorithm&nbsp;history</li>
								<li>Experience of at least one web analytics&nbsp;package</li>
								<li>A high level of proficiency with Microsoft Office, particularly Word, Excel and&nbsp;PowerPoint</li>
								<li>Project and time-management - an initiative to manage projects and your own time&nbsp;efficiently</li>	
							</ul>
							<p><strong>What we'll give you in return</strong></p>
							<ul class="bullet">				
								<li>Training and development at one of Ireland’s leading digital marketing&nbsp;agencies</li>
								<li>Unrivalled career progression</li>
								<li>An inspiring &amp; social team to work with</li>
								<li>Excellent communication and presentation skills.</li>
								<li>Excellent analytical, written and numerical skills</li>
								<li>Excellent organisational skills</li>
								<li>Able to prioritise, sometimes in a pressurized&nbsp;environment</li>
								<li>Experience of MS Excel, Word &amp; PowerPoint</li>	
							</ul>
							<p>If you have any questions about the role please contact Jess Irwin, Head of Search &amp; Analytics: jess(dot)irwin(at)radical(dot)ie</p>
						</div>
						<?php $type='blank'; include('inc/apply_form.php'); ?>
					</div>
				</main>
			</div>
		</div>
	</div>
<?php include('inc/footer.php'); ?>