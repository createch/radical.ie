<?php include('inc/functions.php');
	echo getHead("Social &amp; Community Intern");
?>
	<div id="main">
		<div class="section grid_lvl1">
			<?php $breadcrumb=array(
				'All Jobs&nbsp;&nbsp;&gt;'=>'index.php',
				'Internships 2013&nbsp;&nbsp;&gt;'=>'interning-at-radical.php',
				'Social &amp; Community'=>'social-and-community-intern.php'
			); include('inc/nav.php'); ?>
			<div class="grid_lvl2">
				<main>
					<div class="grid_lvl3">
						<div class="sub_section">
							<hgroup>
								<h3 class="noborder">Social Media Intern</h3>
								<h5 class="margin">We pride ourselves on being at the fore of all things social, and have a great deal of fun in the process. Our team has grown from one person to eight in the space of two years; and we want someone to join the exciting journey that we're&nbsp;on.</h5>
							</hgroup>
							<p>We are looking for an intern to join our ever growing social &amp; community team. The candidate will work closely on a team of social media specialists, alongside our colleagues across all online and offline media disciplines, including experts across all speciality areas in digital (search, display, analytics,&nbsp;etc).</p>
						</div>
						<?php include('inc/about_radical.php'); ?>
						<div class="sub_section">
							<hgroup>
								<h3>Job Spec</h3>
								<h5 class="margin">We are looking for someone to join our team, so that we can further expand the social media services within the Core Media Group, successfully managing client’s day to day social media needs.</h5>
							</hgroup>
							<p>The successful candidate will be passionate about social media, have excellent communication skills, copywriting skills; and the ability to maintain and develop relationships with key influencers in online networks. The role will focus on social media reporting and community&nbsp;moderation.</p>
						</div>
						<div class="sub_section">
							<h3>Skills</h3>
							<p>The following skills are required:</p>
							<ul class="bullet">				
								<li>Experience in social media marketing recommended (not necessary), having applied learned knowledge in previous work placements. Specific experience may include: social media monitoring, brand reputation management, social media account set up and day to day management (Facebook, Twitter, YouTube, etc), customer service query management and social reporting on engagement and response&nbsp;rates.</li> 
								<li>Understanding of the digital landscape in Ireland and abroad; and how all elements are connected (digital media/advertising, search engine marketing, search engine optimisation, email marketing, social media marketing, online public relations and&nbsp;more).</li>
								<li>Has an active role on the online community, by creating content as well as viewing&nbsp;content.</li>
								<li>Excellent writing skills; changing writing styles for Social Networks, Blogs and online press&nbsp;releases.</li>
								<li>Has a passion for all things digital and a yearning to test new and upcoming digital&nbsp;possibilities.</li>
								<li>Ability to multi-task</li>
								<li>Personal qualities should include: social, creativity, determination, confident, works well in teams, works well under pressure, and is ambitious both personally and&nbsp;professionally.</li>
							</ul>
							<p>If you have any questions about the role please contact Emer Lawn, Head of Social Media: emer(dot)lawn(at)radical(dot)ie</p>
						</div>
						<?php $type='social'; include('inc/apply_form.php'); ?>
					</div>
				</main>
			</div>
		</div>
	</div>
<?php include('inc/footer.php'); ?>