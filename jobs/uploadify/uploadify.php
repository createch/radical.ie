<?php
/*
Uploadify
Copyright (c) 2012 Reactive Apps, Ronnie Garcia
Released under the MIT License <http://www.opensource.org/licenses/mit-license.php> 
*/
$verifyToken = md5('unique_salt' . $_POST['timestamp']);

$targetFolder = '/jobs/uploadify/uploads/'; // Relative to the root

if (!empty($_FILES)) {
	$tempFile= $_FILES['Filedata']['tmp_name'];
	$targetPath=$_SERVER['DOCUMENT_ROOT'].$targetFolder.$_GET['session_code'];
	$file_name=$_FILES['Filedata']['name'];
	$targetFile=$targetPath.'/'.$file_name;
	
	// Validate the file type
	$fileTypes = array('doc','DOC','docx','DOCX','pdf','PDF');
	$fileParts = pathinfo($_FILES['Filedata']['name']);
	
	if (in_array($fileParts['extension'],$fileTypes)) {
		if(!is_dir($targetPath)){
			mkdir($targetPath);
			chmod($targetPath,0755);
		}
		move_uploaded_file($tempFile,$targetFile);
		chmod($targetFile,0755);
		echo 'Success';
	} else {
		echo 'Error - Possibly invalid file type.';
	}
}