function MM_goToURL() { //v3.0
  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
}

function MM_validateForm() { //v4.0
  var i,p,q,nm,test,num,min,max,errors='',args=MM_validateForm.arguments;
  for (i=0; i<(args.length-2); i+=3) { test=args[i+2]; val=MM_findObj(args[i]);
    if (val) { nm=val.name; if ((val=val.value)!="") {
      if (test.indexOf('isEmail')!=-1) { p=val.indexOf('@');
        if (p<1 || p==(val.length-1)) errors+='- '+nm+' must contain an e-mail address.\n';
      } else if (test!='R') { num = parseFloat(val);
        if (isNaN(val)) errors+='- '+nm+' must contain a number.\n';
        if (test.indexOf('inRange') != -1) { p=test.indexOf(':');
          min=test.substring(8,p); max=test.substring(p+1);
          if (num<min || max<num) errors+='- '+nm+' must contain a number between '+min+' and '+max+'.\n';
    } } } else if (test.charAt(0) == 'R') errors += '- '+nm+' is required.\n'; }
  } if (errors) alert('The following error(s) occurred:\n'+errors);
  document.MM_returnValue = (errors == '');
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function HS(id,vis){ 
	
	if(vis)
		{
			document.getElementById(id).style.display="";
		}
	else
		{
			document.getElementById(id).style.display="none";
		}

}


//Simulates a form post and off to realex we go!
function loaded() {
  document.realex.target="";
  document.realex.submit();
}

//JS validation for the contact us form on the ir site.
function submitIt(form)
{
	if(!validateName(form.name)) return false;
	if(!validateCompany(form.company)) return false;
	if(!validateURL(form.url)) return false;
	if(!validateEmail(form.email)) return false;
	if(!validatePhone(form.phone)) return false;
	
return true;
}

/* ================================================================================= */	
/* Validate Name */

function validateName(name)
	{
	if(name.value=="")
		{
		alert("You must enter your name");
		name.focus();
		name.select();
		return false;
		}
	else
		{
		return true;
		}
	}
	
/* ================================================================================= */	
/* Validate Company */

function validateCompany(company)
	{
	if(company.value=="")
		{
		alert("You must enter a company name (If not relevant type \"Private User\")");
		company.focus();
		company.select();
		return false;
		}
	else
		{
		return true;
		}
	}

/* ================================================================================= */	
/* Validate Dropdown */

function validateDropdown(dropdown)
	{
	if(dropdown.value=="void")
		{
		alert("You must select an option from the dropdown menu");
		dropdown.focus();
		dropdown.select();
		return false;
		}
	else
		{
		return true;
		}
	}

/* ================================================================================= */	
/* Validate Phone */

function validFone(fone)
	{
	validChars = "0123456789-() ";
	if(fone == "")
		{
		return false;
		}
	for(i=0; i<fone.length; i++)
		{
		temp = "" + fone.substring(i, i+1);
		if (validChars.indexOf(temp) == "-1")
			{
			return false;
			}
		}
	return true
	}

function validatePhone(fone)
	{
	if(!validFone(fone.value))
		{
		alert("Invalid Telephone Number");
		fone.focus();
		fone.select();
		return false;
		}
	else
		{
		return true;
		}
	}
	
/* ================================================================================= */	
/* Validate URL */
	
function validURL(url)
	{
	invalidChars = "!\"�$%^&*()+={}[];@\'#,<>?|`\\�";
	if(url == "")
		{
		return false;
		}
	for(i=0; i<invalidChars.length; i++)
		{
		badChar=invalidChars.charAt(i);
		if (url.indexOf(badChar,0) != -1)
			{
			return false;
			}
		}
	return true;
	}
	
function validateURL(url)
	{
	if(!validURL(url.value))
		{
		alert("Invalid Website Address");
		url.focus();
		url.select();
		return false;
		}
	else
		{
		return true;
		}
	}
	
/* ================================================================================= */	
/* Validate Email Address */

function validEmail(email)
	{
	invalidChars = "/:;,";
	if(email == "")
		{
		return false;
		}
	for(i=0; i<invalidChars.length; i++)
		{
		badChar=invalidChars.charAt(i);
		if (email.indexOf(badChar,0) != -1)
			{
			return false;
			}
		}
	atPos = email.indexOf("@",1);
	if(atPos == -1)
		{
		return false;
		}
	if (email.indexOf("@", atPos+1) != -1)
		{
		return false;
		}
	periodPos = email.indexOf(".",atPos);
	if(periodPos == -1)
		{
		return false;
		}
	if(periodPos+3>email.length)
		{
		return false;
		}
	return true;
	}

function validateEmail(email)
	{	
		if (!validEmail(email.value))
		{
		alert("Invalid Email Address");
		email.focus();
		email.select();
		return false;
		}
	else
		{
		return true;
		}
	}

/* ================================================================================= */	
/* Add to Favourites */

function bookmark(url,title)
	{
	if ((navigator.appName == "Microsoft Internet Explorer") && (parseInt(navigator.appVersion) >= 4)) 
		{
		window.external.AddFavorite(url,title)
		}
	else
		{
		var msg = "Don't forget to bookmark us!";
		if(navigator.appName == "Netscape") msg += " (CTRL+D)";
		alert(msg);
		}
	}

/* ================================================================================= */	
/* popUp window */

function popUp(url,w,h,l,t)
	{
	newWindow = window.open (url,'newWin','scrollbars=no,width='+w+',height='+h+',left='+l+',top='+t);
	}
	
function popFNF(url)
	{
	newWindow = window.open (url,'newWin','scrollbars=no,width=350,height=400,left=150,top=300');
	}
	
/* ================================================================================= */	
/* used on 14 day trail page, only allows a certain number of radio buttons to be pressed.*/

  function limitChecked(obj)
  {
    if (obj.checked) {
		numberChecked = numberChecked + 1
	} else {
		numberChecked = numberChecked - 1
	}
	
    if(numberChecked > maxAllowed)
    {
      alert('You may only check up to '
           + maxAllowed
           + ' engines.');
      obj.checked = false;
      numberChecked--;
    }
  }
  
// 17/11/2008, New Contact US form validation
function contact_us(form)
{
	if(form.name.value == "") { alert("Please enter your name."); form.name.focus(); return false; } 
	if(!form.email.value == "") 
	{ 
		if (!validEmail(form.email.value))
		{
			alert("Please enter a valid email address."); 
			form.email.focus(); 
			return false; 
		}
	} else {
		alert("Please enter your email address."); 
		form.email.focus(); 
		return false; 
	}
	if(form.phone.value == "") { alert("Please enter your phone number."); form.phone.focus(); return false; } 
	if(form.message.value == "") { alert("Please enter a message."); form.message.focus(); return false; } 

	return true;
}
	

