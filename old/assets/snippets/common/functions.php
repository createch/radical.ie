<?php
//janine 160905 extra functions to increase security on forms after php injection attacks
//halts execution of script, displays explanatory message and sends e-mail alert to us

function foilSpam($check, $message)
{
	if(empty($message))
	{
		$message = "Spam attempt denied";
	}
	if(empty($check))
	{
		$check = "Extra check";
	}
	mail("justin@interactivereturn.com", "Interactive Return Spam Attempt", "$check: $message from IP {$_SERVER['REMOTE_ADDR']}", "From: deathtoallspammers@interactivereturn.com\r\n\r\n");
	header("HTTP/1.0 403 Forbidden");
	die($message); 
}

//returns true if string supplied matches a list of suspicious content often used by spammers
function suspiciousInput($value)
{
	return eregi("MIME-Version:|Content-Type:|bcc:|cc:|url=http|href|poker", $value);
}

//cleans user input - removes line feeds, strips slashes
function cleanInput($value)
{
	 return stripslashes(preg_replace("/\r|\n/", "", $value));
}

// Makes one assumption, you call your email post variable email!!!!
// Will return an arrary of cleaned variables for use later in your program, or a string
// With a error message relating to an invalid email address.
function spamdefeat()
{
	// First check, ensure the http referer is on the same domain,
	// Will catch remote posts.
	if (!stristr($_SERVER['HTTP_REFERER'], $_SERVER['HTTP_HOST']))
	{
		foilSpam("IR referrer check", null);
	}
	// Second Check: check that applicant e-mail address is not a site domain address
	// Fairly safe to assume that site owners will not be contacting themselves!
	// wWhen this vulnerability is exploited, an address from the owner's domain is most often used
	if(!empty($_POST['email']) && stristr($_POST['email'], "interactivereturn.com"))
	{
		foilSpam("IR email check", null);
	}
	// Third Check: scan all post variables and look for injection attacks or any
	// other suspicious input in form fields....
	// Declare array to hold all the clean safe data on the otherside!
	$cleanvars = array();
	
	foreach($_POST as $key => $val)
	{
		if(suspiciousInput($val))
		{
			foilSpam("IR suspicious input check", null);
		}
		// Email validate
		if (strtolower($key) == "email")
		{
			if (!eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $val))
			{ 
				return '<p>Please enter a valid email address</p>'; 
			} 
		}
		// Clean the input
		$val = cleanInput($val);
		// Add the data to our cleaned array
		array_push($cleanvars, $val);
	}

return $cleanvars;

}



?>